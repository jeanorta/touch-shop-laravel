<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index');

Route::resource('users','UsersController');
Route::resource('products','ProductsController');
Route::resource('places','PlacesController');
Route::resource('checkouts','CheckoutsController');

Route::get('payments', [
	'as' => 'users.payments', 
	'uses' => 'UsersController@payments'
]);
Route::get('favorites', [
	'as' => 'users.favorites', 
	'uses' => 'UsersController@favorites'
]);

Route::get('products.index', [
	'as' => 'products', 
	'uses' => 'ProductsController@index'
]);
Route::get('products.detail', [
	'as' => 'products.details', 
	'uses' => 'ProductsController@detail'
]);
Route::get('products.delete', [
	'as' => 'products.delete', 
	'uses' => 'ProductsController@delete'
]);
Route::get('places.detail', [
	'as' => 'places.details', 
	'uses' => 'PlacesController@detail'
]);
Route::get('', [
	'as' => 'inicio', 
	'uses' => 'HomeController@index'
]);
Route::get('checkouts.remove', [
	'as' => 'remove', 
	'uses' => 'CheckoutsController@remove'
]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
