var provincias;
var localidades;
var flagOnceSelect=false;
loadListarProvincias();
loadListarLocalidades();
dataselectR();

function loadListarProvincias(){
    var json = $.getJSON("/json/provincias.json");
    setTimeout(function(){
        provincias = json.responseJSON;                    
    }, 500);    
}

function loadListarLocalidades(){
    var json = $.getJSON("/json/localidades.json");
    setTimeout(function(){
        localidad = json.responseJSON;                   
    }, 1000);    
}

function dataselectR(){
    if (provincias==null){
    setTimeout(function(){
                dataselectR();;                  
            }, 1000);
    }else{
        if(flagOnceSelect==false){
            $('.changeAddress_provincia').empty();    
            $('.changeAddress_provincia').append('<label>Provincia</label><select name="provinceName" id="inputProvincia" onchange="loadLocationR(); short();" class="form-control" required><option value="">Seleccionar</option></select>');
            for(var i=0;i<provincias.length;i++){
                    $('#inputProvincia').append('<option value="'+provincias[i].id+'">'+provincias[i].nombre+'</option>');    
            }
            $('.changeAddress_localidad').append('<label>Localidad</label><select name="locationName" id="inputLocalidad" class="form-control" required onchange="short();"><option value="">Seleccionar</option></select>');
            flagOnceSelect=true;
        }
    }    
}

function loadLocationR(){
    var idProvincia = parseInt($('#inputProvincia').val());
    $('#inputLocalidad').empty();
    $('#inputLocalidad').append('<option value="" class="defaultLocation">Seleccionar</option>');
    for(var i=0;i<localidad.length;i++){
        if(localidad[i].provincia_id==idProvincia && localidad[i].nombre!=""){
          $('#inputLocalidad').append('<option value="'+localidad[i].nombre+'|'+localidad[i].codigopostal+'">'+localidad[i].nombre+' ('+localidad[i].codigopostal+')</option>');  
        }        
    }
    if(idProvincia==1){
        $('#envio').empty();
        $('#envio').append('<option value="">Seleccionar</option><option value="Moto">Moto</option><option value="Via CARGO">Via CARGO</option>');
    }else{
        $('#envio').empty();
        $('#envio').append('<option value="">Seleccionar</option><option value="Via CARGO">Via CARGO</option>');
    }    
}

function short(){
    var a =$('#inputProvincia').val();
    if(provincias != null){
        for(i=0; i<provincias.length;i++){
            if(provincias[i].id==a){
                $('#shippingCity').val(provincias[i].nombre);
            }
        }
    }    
    var b =$('#addressInput').val();
    var d = $('#inputLocalidad').val();
    var res = d.split("|"); 
    var e =$('#envio').val();
    var c =res[0]+', '+b+'; '+e;
    $('#shippingAddress').val(c);
    $('#zipCode').val(res[1]);
    $('#billingCity').val(res[1]);

    if($('#envio').val()=='' || $('#addressInput').val()=='' || $('#buyerEmail').val()=='' || $('#inputProvincia').val()=='' || $('#inputLocalidad').val()==''){
        $('#buyNote1').css('display','block');
    }else{  
        $('#buyNote1').css('display','none');
    }
}

function signatureMD5(){ //https://checkout.payulatam.com/ppp-web-gateway-payu
    var caracteres = "0123456789abcdefABCDEF";
    var longitud = 12;
    //var apiLogin = "pRRXKOl8ikMmt9u"; //pruebas
    var apiLogin = "EPeWmp8g0fQ0oUt"; //producción
    //var apiKey = "4Vj8eK4rloUd272L48hsrarnUA"; //pruebas
    var apiKey = "y74yoHjzNWE7nwxRD3i3C8F4tl"; //producción
    //var merchantId = "508029"; //pruebas
    var merchantId = "723679"; //producción
    var referenceCode = rand_code(caracteres,longitud);
    $('#referenceCode').val(referenceCode);
    var amount = $('#totalCar').text();
    var currency = "ARS";
    var signature = md5(apiKey+'~'+merchantId+'~'+referenceCode+'~'+amount+'~'+currency);
    $('#signature').val(signature);
    if(Juser!=null){
        
        $('#buyerFullName').val(Juser.result.name+' '+Juser.result.surname);
        $('#buyerEmail').val(Juser.result.email);
        $('#mobilePhone').val(Juser.result.phone);
        $('#shippingCity').val(Juser.result.city);
        var idUser = Juser.result.idUser;
        var dniUser = Juser.result.dni; 
        var emailUser = Juser.result.email;
        var extra1 = md5(idUser+'~'+dniUser+'~'+emailUser);
        $('#extra1').val(extra1+'|'+placeN);
    }
}











