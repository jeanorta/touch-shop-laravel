var provincias;
var localidades;
var flagOnceSelect=false;
loadListarProvincias();
loadListarLocalidades();
dataselectR();
loadAddress();

function loadAddress(){
    if ($('#inputProvincia').length > 0){    
        var a = $('#state').val();
        if(a != ''){
            if(provincias != null){
                for(i=0; i<provincias.length;i++){
                    if(provincias[i].nombre==a){
                        $('#inputProvincia').val(provincias[i].id);
                    }
                }
            }
            loadLocationR();
            var b = $('#city').val();
            var c = $('#postalCode').val();
            $('#inputLocalidad').val(b+'|'+c);
        }
    }else{
        setTimeout(function(){
            loadAddress();;                  
        }, 1000);    
    }      
}







function loadListarProvincias(){
    var json = $.getJSON("/json/provincias.json");
    setTimeout(function(){
        provincias = json.responseJSON;                    
    }, 500);    
}

function loadListarLocalidades(){
    var json = $.getJSON("/json/localidades.json");
    setTimeout(function(){
        localidad = json.responseJSON;                   
    }, 1000);    
}

function dataselectR(){
	if (provincias==null){
	setTimeout(function(){
                dataselectR();;                  
            }, 1000);
	}else{
		if(flagOnceSelect==false){
	        $('.changeAddress1').empty();    
	        $('.changeAddress1').append('<label>Provincia</label><select name="provinceName" id="inputProvincia" onchange="loadLocationR(); short();" class="form-control"><option value="">Seleccionar</option></select>');
	        for(var i=0;i<provincias.length;i++){
	                $('#inputProvincia').append('<option value="'+provincias[i].id+'">'+provincias[i].nombre+'</option>');    
	        }
	        $('.changeAddress2').append('<label>Localidad</label><select name="locationName" id="inputLocalidad" class="form-control" onchange="short();"><option value="">Seleccionar</option></select>');
	        flagOnceSelect=true;
    	}
	}    
}

function loadLocationR(){
    var idProvincia = parseInt($('#inputProvincia').val());
    $('#inputLocalidad').empty();
    $('#inputLocalidad').append('<option value="" class="defaultLocation">Seleccionar</option>');
    for(var i=0;i<localidad.length;i++){
        if(localidad[i].provincia_id==idProvincia && localidad[i].nombre!=""){
          $('#inputLocalidad').append('<option value="'+localidad[i].nombre+'|'+localidad[i].codigopostal+'">'+localidad[i].nombre+' ('+localidad[i].codigopostal+')</option>');  
        }        
    }    
}

function short(){
    var a =$('#inputProvincia').val();
    if(provincias != null){
        for(i=0; i<provincias.length;i++){
            if(provincias[i].id==a){
                $('#state').val(provincias[i].nombre);
            }
        }
    }  
    var d = $('#inputLocalidad').val();
    var res = d.split("|"); 
    $('#city').val(res[0]);
    $('#postalCode').val(res[1]);    
}