
$('.F').on('change' , function(){
	if($('.F').is(':checked')){
		$('.type1').css('display','block');
	}else{
		$('.type1').css('display','none');
	}
});
$('.M').on('change' , function(){
	if($('.M').is(':checked')){
		$('.type2').css('display','block');
	}else{
		$('.type2').css('display','none');
	}
});
$('.N').on('change' , function(){
	if($('.N').is(':checked')){
		$('.type3').css('display','block');
	}else{
		$('.type3').css('display','none');
	}
});
$('.1Indumentaria').on('change' , function(){
	if($('.1Indumentaria').is(':checked')){
		$('.prod13').css('display','block');
		$('.talle2').css('display','block');
	}else{
		$('.prod13').css('display','none');
		$('.talle2').css('display','none');
	}
});
$('.1Calzado').on('change' , function(){
	if($('.1Calzado').is(':checked')){
		$('.prod12').css('display','block');
		$('.talle1').css('display','block');
	}else{
		$('.prod12').css('display','none');
		$('.talle1').css('display','none');
	}
});
$('.1Accesorios').on('change' , function(){
	if($('.1Accesorios').is(':checked')){
		$('.prod11').css('display','block');
	}else{
		$('.prod11').css('display','none');
	}
});

$('.2Indumentaria').on('change' , function(){
	if($('.2Indumentaria').is(':checked')){
		$('.prod23').css('display','block');
		$('.talle2').css('display','block');
	}else{
		$('.prod23').css('display','none');
		$('.talle2').css('display','none');
	}
});
$('.2Calzado').on('change' , function(){
	if($('.2Calzado').is(':checked')){
		$('.prod22').css('display','block');
		$('.talle1').css('display','block');
	}else{
		$('.prod22').css('display','none');
		$('.talle1').css('display','none');
	}
});
$('.2Accesorios').on('change' , function(){
	if($('.2Accesorios').is(':checked')){
		$('.prod21').css('display','block');
	}else{
		$('.prod21').css('display','none');
	}
});

$('.3Indumentaria').on('change' , function(){
	if($('.3Indumentaria').is(':checked')){
		$('.prod33').css('display','block');
		$('.talle2').css('display','block');
	}else{
		$('.prod33').css('display','none');
		$('.talle2').css('display','none');
	}
});
$('.3Calzado').on('change' , function(){
	if($('.3Calzado').is(':checked')){
		$('.prod32').css('display','block');
		$('.talle1').css('display','block');
	}else{
		$('.prod32').css('display','none');
		$('.talle1').css('display','none');
	}
});
$('.3Accesorios').on('change' , function(){
	if($('.3Accesorios').is(':checked')){
		$('.prod31').css('display','block');
	}else{
		$('.prod31').css('display','none');
	}
});

function bs_input_file() {
	$(".input-file").before(
		function() {
			if ( ! $(this).prev().hasClass('input-ghost') ) {
				var element = $("<input type='file' class='input-ghost' name='imageProduct[]' multiple style='visibility:hidden; height:0; position:fixed;'>");
				element.attr("name",$(this).attr("name"));
				element.change(function(){
					element.next(element).find('input').val((element.val()).split('\\').pop());
				});
				$(this).find("button.btn-choose").click(function(){
					element.click();
				});
				$(this).find("button.btn-reset").click(function(){
					element.val(null);
					$(this).parents(".input-file").find('input').val('');
				});
				$(this).find('input').css("cursor","pointer");
				$(this).find('input').mousedown(function() {
					$(this).parents('.input-file').prev().click();
					return false;
				});
				return element;
			}
		}
	);
}

bs_input_file();
