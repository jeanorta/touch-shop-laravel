    // Change style of navbar on scroll
window.onscroll = function() {myFunction()};
function myFunction() {
    var navbar = document.getElementById("myNavbar");
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
        //navbar.className = "w3-bar" + " w3-card" + " w3-animate-top" + " w3-white";
        $('#myNavbar').addClass("w3-top");
        $('.logoNav').css('display','block');
        $('#myNavbar').removeClass("position");
        $('.bgHoverNav').css('position','fixed');
    } else {
        $('#myNavbar').removeClass("w3-top");
        $('.logoNav').css('display','none');
        $('#myNavbar').addClass("position");
        $('.bgHoverNav').css('position','relative');
        //navbar.className = navbar.className.replace(" w3-card w3-animate-top w3-white", "");
    }
}

function bgshow(){
    $('.bgHoverNav').addClass('show');
    $('.bgHoverNav').removeClass('hide');
}

function bghide(){
    $('.bgHoverNav').addClass('hide');
    $('.bgHoverNav').removeClass('show');
    
}