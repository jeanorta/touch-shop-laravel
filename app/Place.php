<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Conner\Likeable\Likeable;

class Place extends Model
{
    protected $table = "place";
}
