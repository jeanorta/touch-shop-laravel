<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rubro extends Model
{
    protected $table = "rubro";
    protected $fillable = ['descRubro','iconRubro','imageRubro','stateRubro','colorRubro','CodigoColorRubro']; 
}
