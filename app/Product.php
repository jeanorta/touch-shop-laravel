<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Conner\Likeable\Likeable;

class Product extends Model
{
    protected $table = "products";
    protected $fillable = ['codeProduct','nameProduct','descProduct','majorPriceProduct','stock','imageProduct','idRubro','talleS', 'talleM','talleL', 'idPlace']; 
}

// function scopeSearch($query, $name){
// 	return $query->where('name', 'LIKE', "%$name%");
// }
