<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = "tags";
    protected $fillable = ['name'];

    public funtion Article(){
        return $this->belongsToMany('App\Article')->withTimestamps(); // relacion inversa muchos a muchos
    }
}
