<?php

namespace App\Http\Controllers;
//use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Product;
use App\Place;
use App\Rubro;
use App\User;
use App\Favorite;
use App\FavoriteProduct;

class UsersController extends Controller
{
    public function index(Request $request){

    }

    public function create(){
    }

    public function store(Request $request){

    }

    public function show($id){

    }    

    public function edit(Request $request){
        $idRequest = $request->query('id');
        $idRequest = intval($idRequest);
        $typeRequest = $request->query('type');
        $products = Product::orderBy('idProduct', 'ASC')->paginate(4); 
        $rubros = Rubro::orderBy('idRubro', 'ASC')->get(); 
        $places = Place::orderBy('idPlace', 'ASC')->paginate(4);  
        return view('users.edit')->with('rubros', $rubros)->with('products', $products)->with('places', $places)->with('idRequest', $idRequest)->with('typeRequest', $typeRequest);
    }

    public function update(Request $request, $id){
        $user = User::find($id);
        $user -> fill($request->toArray());
        $user->save();
        Flash::success('Su perfil ha sido actualizado con exito')->important();
        $rubros = Rubro::orderBy('idRubro', 'ASC')->get(); 
        return view('users.edit')->with('rubros', $rubros);

    }

    public function favorites(Request $request){
        $idRequest = $request->query('id');
        $idRequest = intval($idRequest);
        if($idRequest!=0 ||$idRequest!=null){
            $likeUserProduct = FavoriteProduct::where('user', auth()->id())->where('idProduct', $idRequest)->get();
            if(count($likeUserProduct->toArray())>0){
                foreach ($likeUserProduct as  $value) {
                    $idLike = $value->id; 
                    $deleteLike = FavoriteProduct::find($idLike);
                    $deleteLike -> delete();
                }                
            }else{
                $like = new FavoriteProduct();
                $like -> idProduct = $idRequest;
                $like -> user = auth()->user()->id;
                $like->save();  
            }
              
        }

        $idRequest2 = $request->query('place');
        $idRequest2 = intval($idRequest2);
        if($idRequest2!=0 ||$idRequest2!=null){
            $likeUserPlace = Favorite::where('user', auth()->id())->where('idPlace', $idRequest2)->get();
            if(count($likeUserPlace->toArray())>0){
                foreach ($likeUserPlace as  $value) {
                    $idLike = $value->id; 
                    $deleteLike = Favorite::find($idLike);
                    $deleteLike -> delete();
                }                
            }else{
                $like2 = new Favorite();
                $like2 -> idPlace = $idRequest2;
                $like2 -> user = auth()->user()->id;
                $like2->save();  
            }
              
        }

        $products = Product::orderBy('idProduct', 'ASC')->get(); 
        $rubros = Rubro::orderBy('idRubro', 'ASC')->get(); 
        $likeUserProduct = FavoriteProduct::orderBy('created_at', 'ASC')->where('user', auth()->id())->get();
        $likeUserPlace = Favorite::orderBy('created_at', 'ASC')->where('user', auth()->id())->get();
        $places = Place::orderBy('idPlace', 'ASC')->get();  
        return view('users.favorites')->with('rubros', $rubros)->with('products', $products)->with('places', $places)->with('idRequest', $idRequest)->with('likeUserProduct', $likeUserProduct)->with('likeUserPlace', $likeUserPlace); 
    }

    public function payments(Request $request){
        $idRequest = $request->query('id');
        $idRequest = intval($idRequest);
        $products = Product::orderBy('idProduct', 'ASC')->paginate(20); 
        $rubros = Rubro::orderBy('idRubro', 'ASC')->get(); 
        $places = Place::orderBy('idPlace', 'ASC')->paginate(20);  
        return view('users.payments')->with('rubros', $rubros)->with('products', $products)->with('places', $places)->with('idRequest', $idRequest);
    }

    public function destroy($id){

    }

}
