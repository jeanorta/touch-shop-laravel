<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Rubro;
use App\Place;
use App\Favorite;

class PlacesController extends Controller
{
    public function index(Request $request){
        $idRequest = $request->query('id');
        $idRequest = intval($idRequest);
        $search = $request->query('search');
        $typeRequest = $request->query('type'); 
        if($search!=''){
           $places = Place::orderBy('idPlace', 'ASC')->where('namePlace', 'LIKE', '%' . $search . '%')->orWhere('addressPlace', 'LIKE', '%' . $search . '%')->paginate(20); 
        }else{
            $places = Place::orderBy('idPlace', 'ASC')->paginate(20);
        }
        $likeUserPlace = Favorite::orderBy('created_at', 'ASC')->where('user', auth()->id())->get(); 
        $rubros = Rubro::orderBy('idRubro', 'ASC')->get();
        $products = Product::orderBy('idProduct', 'ASC')->paginate(20);
        return view('places.index')->with('rubros', $rubros)->with('products', $products)->with('places', $places)->with('idRequest', $idRequest)->with('typeRequest', $typeRequest)->with('likeUserPlace', $likeUserPlace);
    }

    public function detail(Request $request){
        $idRequest = $request->query('id');
        $idRequest = intval($idRequest);
        $places = Place::orderBy('idPlace', 'ASC')->paginate(20);
        $rubros = Rubro::orderBy('idRubro', 'ASC')->get();
        $likeUserPlace = Favorite::orderBy('created_at', 'ASC')->where('user', auth()->id())->get();
        return view('places.detail')->with('places', $places)->with('rubros', $rubros)->with('idRequest', $idRequest)->with('likeUserPlace', $likeUserPlace);
    }
}
