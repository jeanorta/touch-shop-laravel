<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Rubro;
use App\Place;
use Gloudemans\Shoppingcart\Facades\Cart;

class CheckoutsController extends Controller
{
    public function index(Request $request){
        $idRequest = $request->query('id');
        $idRequest = intval($idRequest);
        $typeRequest = $request->query('type');
        $products = Product::orderBy('idProduct', 'ASC')->paginate(4); 
        $rubros = Rubro::orderBy('idRubro', 'ASC')->get(); 
        $places = Place::orderBy('idPlace', 'ASC')->paginate(4);  
        return view('users.checkout')->with('rubros', $rubros)->with('products', $products)->with('places', $places)->with('idRequest', $idRequest)->with('typeRequest', $typeRequest);
    }

    public function store(Request $request){
    	$idProduct = $request->input('id');
    	$qty = $request->input('qty');
    	$size = $request->input('size');
    	$color = $request->input('color');
    	$items = Product::where('idProduct', $idProduct)->get();
    	foreach ($items as $key => $item) {
  		$name = $item -> nameProduct;
  		$price = $item -> majorPriceProduct;
  		$code = $item -> codeProduct;
  		$img = $item-> imageProduct;
  		$place = $item-> idPlace;
		}
		$image = explode("|",$img ); 
              if ($image[0]===""){
                $image[0] = "img/noImageDirectory.png";
              }else{
                $image[0] = env('APP_CONTEXT_FOLDER').$image[0];
              }
    	// Add some items in your Controller.
		Cart::add(['id' => $idProduct, 'name' => $name, 'qty' => $qty, 'price' => $price, 'options' => ['size' => $size, 'color' => $color, 'place' => $place, 'code' => $code, 'image' => $image[0]]]);


        $idRequest = $request->query('id');
        $idRequest = intval($idRequest);
        $typeRequest = $request->query('type');
        $products = Product::orderBy('idProduct', 'ASC')->paginate(4); 
        $rubros = Rubro::orderBy('idRubro', 'ASC')->get(); 
        $places = Place::orderBy('idPlace', 'ASC')->paginate(4);  
        return view('users.checkout')->with('rubros', $rubros)->with('products', $products)->with('places', $places)->with('idRequest', $idRequest)->with('typeRequest', $typeRequest);
    }

    public function remove(Request $request){
    	$rowId = $request->query('row');

    	Cart::remove($rowId);

        $idRequest = $request->query('id');
        $idRequest = intval($idRequest);
        $typeRequest = $request->query('type');
        $products = Product::orderBy('idProduct', 'ASC')->paginate(4); 
        $rubros = Rubro::orderBy('idRubro', 'ASC')->get(); 
        $places = Place::orderBy('idPlace', 'ASC')->paginate(4);  
        return view('users.checkout')->with('rubros', $rubros)->with('products', $products)->with('places', $places)->with('idRequest', $idRequest)->with('typeRequest', $typeRequest);
    }
}
