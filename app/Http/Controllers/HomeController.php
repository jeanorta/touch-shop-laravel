<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Rubro;
use App\Place;
use App\Favorite;
use App\FavoriteProduct;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(Request $request){
        $idRequest = $request->query('id');
        $idRequest = intval($idRequest);
        $products = Product::orderBy('idProduct', 'ASC')->paginate(4); 
        $rubros = Rubro::orderBy('idRubro', 'ASC')->get(); 
        $places = Place::orderBy('idPlace', 'ASC')->paginate(4);  
        $likeUserProduct = FavoriteProduct::orderBy('created_at', 'ASC')->where('user', auth()->id())->get();
        $likeUserPlace = Favorite::orderBy('created_at', 'ASC')->where('user', auth()->id())->get();  
        return view('index')->with('rubros', $rubros)->with('products', $products)->with('places', $places)->with('idRequest', $idRequest)->with('likeUserProduct', $likeUserProduct)->with('likeUserPlace', $likeUserPlace);
    }
}
