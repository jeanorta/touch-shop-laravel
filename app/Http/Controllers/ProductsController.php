<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Product;
use App\Rubro;
use App\Place;
use App\FavoriteProduct;

class ProductsController extends Controller
{
    public function index(Request $request){
        $idRequest = $request->query('id');
        $search = $request->query('search');
        $idRequest = intval($idRequest);
        if ($idRequest=='' && $search=='') {
           $products = Product::orderBy('idProduct', 'ASC')->paginate(20);
        }elseif($idRequest!='' && $search==''){
           $products = Product::orderBy('idProduct', 'ASC')->where('idRubro', 'LIKE', '%,' . $idRequest . ',%')->paginate(20); 
        }elseif($idRequest=='' && $search!=''){
           $products = Product::orderBy('idProduct', 'ASC')->where('nameProduct', 'LIKE', '%' . $search . '%')->paginate(20); 
        }
        $rubros = Rubro::orderBy('idRubro', 'ASC')->get(); 
        $places = Place::orderBy('idPlace', 'ASC')->paginate(20); 
        $likeUserProduct = FavoriteProduct::orderBy('created_at', 'ASC')->where('user', auth()->id())->get();	
    	return view('products.index')->with('rubros', $rubros)->with('products', $products)->with('places', $places)->with('idRequest', $idRequest)->with('likeUserProduct', $likeUserProduct);
    }

    public function detail(Request $request){
        $idRequest = $request->query('id');
        $idRequest = intval($idRequest);
        $products = Product::where('idProduct',$idRequest)->get();
        $rubros = Rubro::orderBy('idRubro', 'ASC')->get();
        $likeUserProduct = FavoriteProduct::orderBy('created_at', 'ASC')->where('user', auth()->id())->get();
        return view('products.detail')->with('products', $products)->with('rubros', $rubros)->with('idRequest', $idRequest)->with('likeUserProduct', $likeUserProduct);
    }

    public function create(){
        $products = Product::orderBy('idProduct', 'ASC')->get(); 
        $rubros = Rubro::orderBy('idRubro', 'ASC')->get(); 
        $places = Place::orderBy('namePlace', 'ASC')->get();
        return view('products.create')->with('products', $products)->with('rubros', $rubros)->with('places', $places);
    }

    public function store(Request $request){
        $product = new Product();
        $product -> codeProduct = $request->input('codeProduct');
        $product -> nameProduct = $request->input('nameProduct');
        $product -> descProduct = $request->input('descProduct');
        $product -> majorPriceProduct = $request->input('majorPriceProduct');
        $product -> idPlace = $request->input('idPlace');
        $images = $request->file('imageProduct');
        $path = public_path().'\img\context\\';
        $i = 1;
        $name = '';
        foreach($images as $image) {
            $filename = 'product'.time().'-'.$i.'.'.$image -> getClientOriginalExtension();
            $name = $name.'product'.time().'-'.$i.'.'.$image -> getClientOriginalExtension().'|';
            $image->move($path,$filename);
            $i++;
        }
        $product -> imageProduct = $name;
        $rubros = implode(',',$request->input('rubro'));
        $product -> idRubro = $rubros;        
        $product->save();
        Flash::success('Producto Agregado exitosamente')->important();
        return redirect()->route('products.create');

    }

    public function show($id){
        $products = Product::orderBy('idProduct', 'ASC')->paginate(25); 
        $rubros = Rubro::orderBy('idRubro', 'ASC')->get(); 
        $places = Place::orderBy('idPlace', 'ASC')->get();
        return view('products.show')->with('products', $products)->with('rubros', $rubros)->with('places', $places);
    }

    public function edit($id){
        $products = Product::orderBy('idProduct', 'ASC')->get(); 
        $rubros = Rubro::orderBy('idRubro', 'ASC')->get(); 
        $places = Place::orderBy('idPlace', 'ASC')->get();
        return view('products.edit')->with('products', $products)->with('rubros', $rubros)->with('places', $places)->with('id', $id);
    }

    public function update(Request $request, $id){
        $editProduct = Product::where('idProduct', $id)->get();
        $editProduct->first()->codeProduct = $request->input('codeProduct');
        $editProduct->first()->nameProduct = $request->input('nameProduct');
        $editProduct->first()->descProduct = $request->input('descProduct');
        $editProduct->first()->majorPriceProduct = $request->input('majorPriceProduct');
        $editProduct->first()->idPlace = $request->input('idPlace');
        if (null !==$request->input('rubro')) { 
            $rubros = implode(',',$request->input('rubro'));
            $editProduct->first()->idRubro = $rubros; 
        } 
        if (null !==$request->file('imageProduct')) { 
            $images = $request->file('imageProduct');
            $imagesOld = $request->input('imageOld');
            $imagesOld = explode('|',$imagesOld);
            $path = public_path().'\img\context\\';
            foreach ($imagesOld as $old) {
                if(\File::exists($path.$old)){
                  \File::delete($path.$old);
                }
            }            
            $i = 1;
            $name = '';
            foreach($images as $image) {
                $filename = 'product'.time().'-'.$i.'.'.$image -> getClientOriginalExtension();
                $name = $name.'product'.time().'-'.$i.'.'.$image -> getClientOriginalExtension().'|';
                $image->move($path,$filename);
                $i++;
            }
            $editProduct->first()->imageProduct = $name; 
        } 
        //dd($editProduct); 
        DB::table('products')->where('idProduct', $id)->update($editProduct->first()->toArray());
        $products = Product::orderBy('idProduct', 'ASC')->paginate(25); 
        $rubros = Rubro::orderBy('idRubro', 'ASC')->get(); 
        $places = Place::orderBy('idPlace', 'ASC')->get();
        Flash::success('El articulo ha sido actualizado con exito')->important();
        return view('products.show')->with('products', $products)->with('rubros', $rubros)->with('places', $places);
    }

    public function delete(Request $request){
        $id = $request->query('id');
        $product = Product::where('idProduct', $id)->get();
        //dd($product);
        DB::table('products')->where('idProduct', $id)->delete();
        Flash::error('El articulo: '.$product->first()->nameProduct.' ha sido borrado con exito')->important();  
        $imagesOld = explode('|',$product->first()->imageProduct); 
        $path = public_path().'\img\context\\';
        foreach ($imagesOld as $old) {
            if(\File::exists($path.$old)){
                \File::delete($path.$old);
            }
        }
        return redirect()->route('products.show',auth()->id());
    }

    
}
