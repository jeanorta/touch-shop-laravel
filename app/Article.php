<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = "articles";
    protected $fillable = ['title','content','category_id','user_id'];

    public funtion category(){
    	return $this->belongsTo('App\Category'); //relacion inversa uno a uno
    }

    public funtion user(){
    	return $this->belongsTo('App\User'); //relacion inversa uno a uno
    }

    public funtion image(){
        return $this->hasMany('App\Image'); // relacion directa uno a muchos
    }

    public funtion tag(){
        return $this->belongsToMany('App\Tag'); // relacion directa muchos a muchos
    }
}
