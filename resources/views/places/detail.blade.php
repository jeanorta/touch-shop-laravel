@extends('layouts.main')
@section('title','Detalle Local')
@section('content')

  <link href="theme/css/style.css" rel="stylesheet" type="text/css" media="all" />
  <link href="theme/css/form.css" rel="stylesheet" type="text/css" media="all" />
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
  


<!--details-product-slider-->
        <!-- Include the Etalage files -->
          <link rel="stylesheet" href="theme/css/etalage.css">
          <script src="theme/js/jquery.etalage.min.js"></script>
        <!-- Include the Etalage files -->
        <script>
            jQuery(document).ready(function($){
      
              $('#etalage').etalage({
                thumb_image_width: 300,
                thumb_image_height: 300,
                
                show_hint: true,
                click_callback: function(image_anchor, instance_id){
                  alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
                }
              });
              // This is for the dropdown list example:
              $('.dropdownlist').change(function(){
                etalage_show( $(this).find('option:selected').attr('class') );
              });

          });
        </script>
<script type="text/javascript" src="theme/js/move-top.js"></script>
<script type="text/javascript" src="theme/js/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
      $(".scroll").click(function(event){   
        event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
      });
    });
</script>  
   
       <div class="col-sm-12 w3-panel w3-card-4 w3-white">
         
        @foreach($places as $place)
        @if($place -> idPlace === $idRequest)
        <?php  
              $img = substr( $place->imagePlace , 0 , -1);
              $place2 = explode("|", $img); 
              if ($place2[0]===""){
                $place2[0] = "../noImageDirectory.png";
              }else{
                //$product2[0] = env('APP_CONTEXT_FOLDER_LOCAL').$produplace
              }
            ?>
        <div class="row" style="margin-top: 20px;">
          <div class="col-sm-3">
              <!-- start product_slider -->
             <ul id="etalage">
              @foreach($place2 as $image)
              <li>
                <img class="etalage_thumb_image" src="{{env('APP_CONTEXT_FOLDER_LOCAL').$image}}" />
                <img class="etalage_source_image" src="{{env('APP_CONTEXT_FOLDER_LOCAL').$image}}" />
              </li>
              @endforeach
            </ul> 
            <!-- end product_slider -->
          </div>
          <div class="col-sm-4" style="margin-left: 10px;">
            <h3 class="m_3" id="nameP">{{ $place -> namePlace}}</h3>  
            <div class="btn_form">
              {!! Form::open([ 'method' => 'POST','class' => 'row', 'route' => 'checkouts.store']) !!}
              <input type="hidden" name="id" value="{{ $place -> idPlace}}">
              <input type="hidden" id="lat" value="{{ $place -> latPlace}}">
              <input type="hidden" id="long" value="{{ $place -> lngPlace}}">
              <div class="m_3">
                <div class="form-group col-sm-12">
                  <h4 class="m_9">Ciudad: {{$place -> cityPlace}}</h4>
                  <h4 class="m_9">Barrio: {{$place -> townPlace}}</h4>
                  <h4 class="m_9">Dirección: {{$place -> addressPlace}}</h4>
                  <h4 class="m_9">Teléfono: {{$place -> phonePlace}}</h4>
                  <h4 class="m_9">Correo: {{$place -> emailPlace}}</h4>
                  
                </div>
              </div>
              
              {!! Form::close() !!}
            </div>
            <h4 class="m_9"><a href="{{route('users.favorites',['place' => $place -> idPlace])}}">
              <?php $flatPlace = true;?>
                      @foreach($likeUserPlace as $like)                     
                      @if($place -> idPlace == $like -> idPlace)
                      <i class="fa fa-heart"> Quitar de Favoritos</i>
                      <?php $flatPlace = false;?>                      
                      @endif
                      @endforeach
                      @if($flatPlace)
                      <i class="fa fa-heart-o"> Agregar a Favoritos</i>
                      @endif</a></h4>
          </div>
          <div class="col-sm-4">
            <h3 class="m_3">Mapa </h3>
            <div id='map'></div>
          </div>
        </div>
         <div class="wrap">          

    <div class="cont span_2_of_3">
        

      <div class="clear"></div>
     
     
      <script type="text/javascript">
     $(window).load(function() {
      $("#flexiselDemo1").flexisel();
      $("#flexiselDemo2").flexisel({
        enableResponsiveBreakpoints: true,
          responsiveBreakpoints: { 
            portrait: { 
              changePoint:480,
              visibleItems: 1
            }, 
            landscape: { 
              changePoint:640,
              visibleItems: 2
            },
            tablet: { 
              changePoint:768,
              visibleItems: 3
            }
          }
        });
    
      $("#flexiselDemo3").flexisel({
        visibleItems: 5,
        animationSpeed: 1000,
        autoPlay: true,
        autoPlaySpeed: 3000,        
        pauseOnHover: true,
        enableResponsiveBreakpoints: true,
          responsiveBreakpoints: { 
            portrait: { 
              changePoint:480,
              visibleItems: 1
            }, 
            landscape: { 
              changePoint:640,
              visibleItems: 2
            },
            tablet: { 
              changePoint:768,
              visibleItems: 3
            }
          }
        });
        
    });
  </script>
  <script type="text/javascript" src="theme/js/jquery.flexisel.js"></script> 
  
          
          
            <!-- end product_slider -->
     </div>

      
    
     <div class="clear"></div>
   </div>
      @endif
      @endforeach
     </div>
    
       <script type="text/javascript">
      $(document).ready(function() {
      
        var defaults = {
            containerID: 'toTop', // fading element id
          containerHoverID: 'toTopHover', // fading element hover id
          scrollSpeed: 1200,
          easingType: 'linear' 
        };
        
        
        $().UItoTop({ easingType: 'easeOutQuart' });
        
      });
    </script>
        <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>

        <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.46.0/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.46.0/mapbox-gl.css' rel='stylesheet' />
    <style>
        #map { /*position:absolute; top:0; bottom:0; width:80%;*/  
        height: 400px; }
    </style>

<script>
  map();
function map(){

setTimeout(function(){  
if($('#lat').val()===''){
  map();
}else{
  var lat =  $('#lat').val();
  var long =  $('#long').val();
  var nameP =  $('#nameP').text();
mapboxgl.accessToken = 'pk.eyJ1IjoiamVhbm9ydGEiLCJhIjoiY2pocW91N2pyNHAwdjNkbjM0ZHJ3eGEyMyJ9.urspGc7q5_G1JF6MAAV0FQ';
var map = new mapboxgl.Map({
    style: 'mapbox://styles/mapbox/light-v9',
    center: [long, lat],
    zoom: 15.5,
    pitch: 45,
    bearing: -17.6,
    container: 'map'
});

// The 'building' layer in the mapbox-streets vector source contains building-height
// data from OpenStreetMap.
map.on('load', function() {
    // Insert the layer beneath any symbol layer.
    var layers = map.getStyle().layers;

    var labelLayerId;
    for (var i = 0; i < layers.length; i++) {
        if (layers[i].type === 'symbol' && layers[i].layout['text-field']) {
            labelLayerId = layers[i].id;
            break;
        }
    }

    map.addLayer({
        'id': '3d-buildings',
        'source': 'composite',
        'source-layer': 'building',
        'filter': ['==', 'extrude', 'true'],
        'type': 'fill-extrusion',
        'minzoom': 15,
        'paint': {
            'fill-extrusion-color': '#aaa',

            // use an 'interpolate' expression to add a smooth transition effect to the
            // buildings as the user zooms in
            'fill-extrusion-height': [
                "interpolate", ["linear"], ["zoom"],
                15, 0,
                15.05, ["get", "height"]
            ],
            'fill-extrusion-base': [
                "interpolate", ["linear"], ["zoom"],
                15, 0,
                15.05, ["get", "min_height"]
            ],
            'fill-extrusion-opacity': .6
        }
    }, labelLayerId);

    // punto
    map.addLayer({
        "id": "points",
        "type": "symbol",
        "source": {
            "type": "geojson",
            "data": {
                "type": "FeatureCollection",
                "features": [{
                    "type": "Feature",
                    "geometry": {
                        "type": "Point",
                        "coordinates": [long, lat]
                    },
                    "properties": {
                        "title": nameP,
                        "icon": "monument"
                    }
                }]
            }
        },
        "layout": {
            "icon-image": "{icon}-15",
            "text-field": "{title}",
            "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
            "text-offset": [0, 0.6],
            "text-anchor": "top"
        }
    });
    // fin
    // Add zoom and rotation controls to the map.
    map.addControl(new mapboxgl.NavigationControl());
});

}               
    }, 1000);


}
</script>
@endsection









