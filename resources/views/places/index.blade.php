
@extends('layouts.main')
@section('title','Locales')
@section('content')
  <div class="container">
    <div class="row">            
      <div class="col-sm-12 w3-panel w3-card-4 w3-white">
        <div class="row w3-container">
          <div class="col-sm-3" >
            <div class="row">
              <!-- The Filter Section -->
              <div class="col-sm-11 w3-panel" > 
                {!! Form::open([ 'method' => 'GET','class' => 'row', 'route' => 'places.index']) !!}
                <input class="form-control col-sm-10" id="myInput" name="search"  type="text" placeholder="Buscar.."> 
                <button class="btn palette-pumpkin col-sm-2"><i class="fas fa-search"></i></button>
                {!! Form::close()!!} 
              </div>
            </div>
          </div><div class="col-sm-3 w3-panel" style="margin-top: 20px;"><h6> {{'Locales: '.$places->Total() .' resultados'}}</h6></div>
        </div>
        <hr style="margin-top: 0px;"> 
        <div class="w3-container">          
          {{-- directorio de locales --}}            
          <div class="row" id="directory">            
            @foreach($places as $place)
              <?php  
                $place2 = explode("|",$place->imagePlace ); 
                if ($place2[0]===""){
                  $place2[0] = "img/noImageDirectory.png";
                }else{
                  $place2[0] = env('APP_CONTEXT_FOLDER').$place2[0];
                }
              ?>             
              <div class="col-sm-3">
                <a href="{{ route('places.details', ['id' => $place -> idPlace]) }}"> 
                  <div class="row"><div class="col-sm-11 w3-panel w3-card-2 w3-hover-shadow" style="height: 330px;"><center><div style="min-height: 200px;"><img src="{{$place2[0]}}" class="img-catalg img-responsive"></div></center><span style="font-size: 11; font-weight: bold;">{{ $place -> namePlace}}</span><br><div style="font-size: 10px; color: #858585; height: 70px;">{{ $place -> addressPlace}}<br><strong>mail: </strong> {{ $place -> emailPlace}}<br><strong>Telf.: </strong> {{ $place -> phonePlace}}</div><a href="{{route('users.favorites',['place' => $place -> idPlace])}}" class="w3-right">
                    <?php $flatPlace = true;?>
                    @foreach($likeUserPlace as $like)                     
                    @if($place -> idPlace == $like -> idPlace)
                    <i class="fa fa-heart"></i>
                    <?php $flatPlace = false;?>                      
                    @endif
                    @endforeach
                    @if($flatPlace)
                    <i class="fa fa-heart-o"></i>
                    @endif
                    
                    
                  </a></div></div>
                </a>
              </div>
            @endforeach
            <div id=paginationDirectory class="col-sm-12">
              {!! $places->appends(['id' => $idRequest])->render() !!} 
            </div>            
          </div>
          {{-- fin directorio --}}
        </div>
      </div>          
    </div>
  </div>
  <!-- The Contact Section -->
  <div class="w3-container w3-content w3-padding-64 w3-black w3-opacity-min2" style="max-width:800px; margin-bottom:16px;" id="contact">
    <h2 class="w3-wide w3-center">CONTACTO</h2>
    {{-- <p class="w3-opacity w3-center"><i>Fan? Drop a note!</i></p> --}}
    <div class="w3-row w3-padding-32">
      <div class="w3-col m6 w3-large w3-margin-bottom">
        <i class="fa fa-map-marker" style="width:30px"></i> CABA, BA<br>
        {{-- <i class="fa fa-phone" style="width:30px"></i> Phone: +00 151515<br> --}}
        <i class="fa fa-envelope" style="width:30px"> </i> Email: mail@mail.com<br>
      </div>
      <div class="w3-col m6">
        <form action="/action_page.php" target="_blank">
          <div class="w3-row-padding" style="margin:0 -16px 8px -16px">
            <div class="w3-half">
              <input class="w3-input w3-border" type="text" placeholder="Name" required name="Name">
            </div>
            <div class="w3-half">
              <input class="w3-input w3-border" type="text" placeholder="Email" required name="Email">
            </div>
          </div>
          <input class="w3-input w3-border" type="text" placeholder="Message" required name="Message">
          <button class="w3-button w3-white w3-section w3-right" type="submit">ENVIAR</button>
        </form>
      </div>
    </div>
  </div>  
  <!-- End Page Content -->
@endsection
<script type="text/javascript" src="{{asset('js/index.js')}}"></script>