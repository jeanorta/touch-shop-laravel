<!-- Navbar -->
<div style="height: 100px; background: #8080808a;"><img src="{{asset('img/logo.png')}}" class="logo"></div>
<div id="myNavbar" class="position" style="z-index: 1001; width: 100%;">
  <div class="w3-bar w3-black w3-card" >
    <a class="w3-bar-item w3-button w3-padding-large w3-hide-medium w3-hide-large w3-right" href="javascript:void(0)" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
    <img src="{{asset('img/logo.png')}}" class="logoNav" style="display: none">
    <a href="{{ route('inicio') }}" class="w3-bar-item w3-button w3-padding-large">INICIO</a>
    
    <div class="w3-dropdown-hover w3-hide-small" style="z-index: 1000;" onmouseover="bgshow();" onmouseout="bghide();">
      <button class="w3-padding-large w3-button" title="More">LOCALES <i class="fas fa-caret-down"></i></button>     
      <div class="w3-dropdown-content w3-bar-block w3-card-4">
        <a href="{{ route('places.index') }}" class="w3-bar-item w3-button">AVELLANEDA</a>
        <a href="{{ route('places.index') }}" class="w3-bar-item w3-button">LA SALADA (proximamente)</a>
      </div>
    </div>
    {{-- Rubros --}}      
    <div class="w3-dropdown-hover w3-hide-small" style="z-index: 11;" onmouseover="bgshow();" onmouseout="bghide();">
      
      <button class="w3-padding-large w3-button" title="More"><a href="{{ route('products.index', ['id' => 1]) }}">MUJER</a><i class="fas fa-caret-down"></i></button>     
      <div class="w3-dropdown-content w3-bar-block w3-card-4 w3-row-padding" style="width: 100%; position: absolute;left: 0;">
        <div class="w3-col l2 m4">&nbsp;</div>
        <div class="w3-col l2 m4">
          <?php $init = true; ?>
          @foreach($rubros as $rubro)
            <?php 
              $idRubro = $rubro -> idRubro;                
              $codigo = $rubro -> iconRubro;
              $array = explode(".", $codigo);
            ?> 
            @if($array[1] !== '000' && $array[2]==='000' && $array[0] == 1) 
              @if($init === true)
                <h6><strong>{{ $rubro -> descRubro}}</strong></h6>
                <?php $init= false;?>
              @else 
                <a href="{{ route('products.index', ['id' => $verTodo]) }}" class="w3-bar-item item2 w3-button"><strong>Ver todos</strong></a>
                </div>                 
                <div class="w3-col l2 m4">              
                <h6><strong>{{ $rubro -> descRubro}}</strong></h6>
              @endif
              <?php $verTodo = $rubro -> idRubro;?>
            @endif
            @if($array[1] !== '000' && $array[2]!=='000' && $array[0] == 1)                  
              <a href="{{ route('products.index', ['id' => $rubro -> idRubro]) }}" class="w3-bar-item item2 w3-button">{{ $rubro -> descRubro}}</a>
            @endif
          @endforeach 
          <a href="{{ route('products.index', ['id' => $verTodo]) }}" class="w3-bar-item item2 w3-button"><strong>Ver todos</strong></a>
        </div>         
      </div>
    </div>
    <div class="w3-dropdown-hover w3-hide-small" style="z-index: 11" onmouseover="bgshow();" onmouseout="bghide();">
      <button class="w3-padding-large w3-button" title="More"><a href="{{ route('products.index', ['id' => 42]) }}">HOMBRE</a> <i class="fas fa-caret-down"></i></button>     
      <div class="w3-dropdown-content w3-bar-block w3-card-4 w3-row-padding" style="width: 100%; position: absolute;left: 0;">
        <div class="w3-col l2 m4">&nbsp;</div>
        <div class="w3-col l2 m4">
         <?php $init = true; ?>
          @foreach($rubros as $rubro)
            <?php 
              $idRubro = $rubro -> idRubro;                
              $codigo = $rubro -> iconRubro;
              $array = explode(".", $codigo);
            ?> 
            @if($array[1] !== '000' && $array[2]==='000' && $array[0] == 2) 
              @if($init === true)
                <h6><strong>{{ $rubro -> descRubro}}</strong></h6>
                <?php $init= false;?>
              @else 
                <a href="{{ route('products.index', ['id' => $verTodo]) }}" class="w3-bar-item item2 w3-button"><strong>Ver todos</strong></a>
                </div>                 
                <div class="w3-col l2 m4">              
                <h6><strong>{{ $rubro -> descRubro}}</strong></h6>
              @endif
              <?php $verTodo = $rubro -> idRubro;?>
            @endif
            @if($array[1] !== '000' && $array[2]!=='000' && $array[0] == 2)                  
              <a href="{{ route('products.index', ['id' => $rubro -> idRubro]) }}" class="w3-bar-item item2 w3-button">{{ $rubro -> descRubro}}</a>
            @endif
          @endforeach 
          <a href="{{ route('products.index', ['id' => $verTodo]) }}" class="w3-bar-item item2 w3-button"><strong>Ver todos</strong></a>
        </div>        
      </div>
    </div>
    <div class="w3-dropdown-hover w3-hide-small" style="z-index: 11" onmouseover="bgshow();" onmouseout="bghide();">
      <button class="w3-padding-large w3-button" title="More"><a href="{{ route('products.index', ['id' => 80]) }}">NIÑOS</a> <i class="fas fa-caret-down"></i></button>     
      <div class="w3-dropdown-content w3-bar-block w3-card-4 w3-row-padding" style="width: 100%; position: absolute;left: 0;">
        <div class="w3-col l2 m4">&nbsp;</div>
        <div class="w3-col l2 m4">
          <?php $init = true; ?>
          @foreach($rubros as $rubro)
            <?php                 
              $codigo = $rubro -> iconRubro;
              $array = explode(".", $codigo);
            ?> 
            @if($array[1] !== '000' && $array[2]==='000' && $array[0] == 3) 
              @if($init === true)
                <h6><strong>{{ $rubro -> descRubro}}</strong></h6>
                <?php $init= false;?>
              @else 
                <a href="{{ route('products.index', ['id' => $verTodo]) }}" class="w3-bar-item item2 w3-button"><strong>Ver todos</strong></a>
                </div>                 
                <div class="w3-col l2 m4">              
                <h6><strong>{{ $rubro -> descRubro}}</strong></h6>
              @endif
              <?php $verTodo = $rubro -> idRubro;?>
            @endif
            @if($array[1] !== '000' && $array[2]!=='000' && $array[0] == 3)                  
              <a href="{{ route('products.index', ['id' => $rubro -> idRubro]) }}" class="w3-bar-item item2 w3-button">{{ $rubro -> descRubro}}</a>
            @endif
          @endforeach 
          <a href="{{ route('products.index', ['id' => $verTodo]) }}" class="w3-bar-item item2 w3-button"><strong>Ver todos</strong></a>
        </div>        
      </div>
    </div>
    <div class="w3-dropdown-hover w3-hide-small" style="z-index: 11" onmouseover="bgshow();" onmouseout="bghide();">
      <button class="w3-padding-large w3-button" title="More"><a href="{{ route('products.index', ['id' => 93]) }}">DEPORTES</a><i class="fas fa-caret-down"></i></button>     
      <div class="w3-dropdown-content w3-bar-block w3-card-4 w3-row-padding" style="width: 100%; position: absolute;left: 0;">
        <div class="w3-col l2 m4">&nbsp;</div>
        <div class="w3-col l2 m4">
          <?php $init = true; ?>
          @foreach($rubros as $rubro)
            <?php                 
              $codigo = $rubro -> iconRubro;
              $array = explode(".", $codigo);
            ?> 
            @if($array[1] !== '000' && $array[2]==='000' && $array[0] == 4)

              @if($init === true)
                <h6><strong>{{ $rubro -> descRubro}}</strong></h6>
                <?php $init= false;?>
              @else
                <a href="{{ route('products.index', ['id' => $verTodo]) }}" class="w3-bar-item item2 w3-button"><strong>Ver todos</strong></a>
                </div>                 
                <div class="w3-col l2 m4">              
                <h6><strong>{{ $rubro -> descRubro}}</strong></h6>                  
              @endif
              <?php $verTodo = $rubro -> idRubro;?>
            @endif
            @if($array[1] !== '000' && $array[2]!=='000' && $array[0] == 4)                  
              <a href="{{ route('products.index', ['id' => $rubro -> idRubro]) }}" class="w3-bar-item item2 w3-button">{{ $rubro -> descRubro}}</a>
            @endif
          @endforeach 
          <a href="{{ route('products.index', ['id' => $verTodo]) }}" class="w3-bar-item item2 w3-button"><strong>Ver todos</strong></a>
        </div>        
      </div>
    </div>
    {{--  --}}
    <!-- Authentication Links -->
    @if (Auth::guest())    
      <a href="{!!route('login')!!}" class="w3-padding-large w3-hover-red w3-hide-small w3-right">Entrar <i class="fas fa-sign-in-alt"></i></a>
    @else
      
      <div class="w3-dropdown-hover w3-hide-small w3-right">
        <button class="w3-padding-large w3-button" title="More">{{ Auth::user()->name }} <i class="fas fa-caret-down"></i></button>     
        <div class="w3-dropdown-content w3-bar-block w3-card-4">
          <a href="{{ route('users.edit',['id' => Auth::user()->id]) }}" class="w3-bar-item w3-button">Mi Perfil</a>
          @if (Auth::user()->email==='jeanorta@usb.ve') 
          <a href="{{ route('products.show',['id' => Auth::user()->id]) }}" class="w3-bar-item w3-button">Productos</a>
          @endif
          <a href="{{ route('logout') }}" class="w3-bar-item w3-button" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
            Salir
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
        </div>
      </div>
      <a href="{{ route('checkouts.index') }}" class="w3-bar-item item3 w3-button  w3-hide-small w3-right"><i class="fa fa-shopping-cart"></i></a>
      <a href="{{ route('users.payments') }}" class="w3-bar-item item3 w3-button  w3-hide-small w3-right"><i class="fa fa-credit-card"></i></a>      
      <a href="{{ route('users.favorites') }}" class="w3-bar-item item3 w3-button  w3-hide-small w3-right"><i class="fa fa-heart"></i></a>
    @endif
    {{--  --}}
    {{-- <a href="{!!route('login')!!}" class="w3-padding-large w3-hover-red w3-hide-small w3-right">Entrar <i class="fas fa-sign-in-alt"></i></a> --}}
  </div>
</div>
<!--{ route('admin.users.create')}} ruta para crear usuario-->
<!-- Navbar on small screens (remove the onclick attribute if you want the navbar to always show on top of the content when clicking on the links) -->
<div id="navDemo" class="w3-bar-block w3-black w3-hide w3-hide-large w3-hide-medium w3-top" style="margin-top:46px">
  <a href="#band" class="w3-bar-item w3-button w3-padding-large" onclick="myFunction()">LOCALES</a>
  <a href="#tour" class="w3-bar-item w3-button w3-padding-large" onclick="myFunction()">MUJER</a>
  <a href="#contact" class="w3-bar-item w3-button w3-padding-large" onclick="myFunction()">HOMBRE</a>
  <a href="#" class="w3-bar-item w3-button w3-padding-large" onclick="myFunction()">INICIO</a>
</div>
<script type="text/javascript" src="{{asset('js/nav.js')}}"></script>