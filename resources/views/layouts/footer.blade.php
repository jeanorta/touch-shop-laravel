<!-- Footer -->
<footer class="w3-container w3-padding-32 w3-center w3-opacity w3-light-grey w3-xlarge">
  <i class="fab fa-facebook-square w3-hover-opacity"></i>
  <i class="fab fa-instagram w3-hover-opacity"></i>
  <p class="w3-medium">Desarrollado por Touch & Shop, &copy; 2018</p>
</footer>