
<!DOCTYPE html>
<html>
<head>
  <title>@yield('title') | Touch and Shop</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <!-- Styles CSS -->
  <link rel="stylesheet" href="{{asset('css/styles.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/bootstrap4/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/w3/css/w3.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/fontawesome/css/all.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/fontawesome/css/v4-shims.css')}}">
  <!-- JavaScript -->
  <script type="text/javascript" src="{{asset('js/jquery-3.2.1.min.js')}}"></script> 
  <script type="text/javascript" src="{{asset('plugins/bootstrap4/js/bootstrap.min.js')}}"></script> 
  <script type="text/javascript" src="{{asset('theme/js/move-top.js')}}"></script>
  <script type="text/javascript" src="{{asset('theme/js/easing.js')}}"></script>
</head>
<body>
  @include('layouts.nav')
  <section>{{-- style="background: rgba(0, 0, 0, 0.71);" --}}
  	<!-- Principal Section -->
    <div class="bgHoverNav" style="display: none;"></div>
    @yield('content')
  </section>  
	@include('layouts.footer')
</body>
</html>
