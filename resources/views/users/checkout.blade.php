@extends('layouts.main')
@section('title','Carrito de Compras')
@section('content')
<div class="w3-white">
	<div class="container">

        <h2 class="col-sm-12">Carro de compras <hr/></h2> 
        <h3 class="col-sm-12">Descripción de Artículos</h3> 
        <table class="table table-bordered table-hover">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Imagen</th>
      <th scope="col">Código</th>
      <th scope="col">Local</th>
      <th scope="col">Nombre Producto</th>
      <th scope="col">Talle</th>
      <th scope="col">Color</th>
      <th scope="col">Precio $ U.</th>
      <th scope="col">Cantidad</th>
      <th scope="col">SubTotal</th>
      <th scope="col">Acciones</th>
    </tr>
  </thead>
  <tbody>
  	<?php $item = 1; $desc= "Productos:";$extra2='';$extra3='';$place ='';?>
  	@foreach(Cart::content() as $row)
    <tr>
      <th scope="row">{{$item}}</th>
      <?php 
      	$rowid=$row->rowId; 
	    $item++; 
	    $desc=$desc.$row->options->code.','.$row->options->size.','.$row->options->color.';';
	    $extra2=$extra2.$row->price.',';
	    $extra3=$extra3.$row->qty.',';
	    $place = $place.$row->options->place.',';
      ?>
      <td><img src="{{$row->options->has('image') ? $row->options->image : ''}}" style="max-height: 150px;"></td>
      <td>{{$row->options->has('code') ? $row->options->code : ''}}</td>
      <td>{{$row->options->has('place') ? $row->options->place : ''}}</td>
      <td>{{$row->name}}</td>
      <td>{{$row->options->has('size') ? $row->options->size : ''}}</td>
      <td>{{$row->options->has('color') ? $row->options->color : ''}}</td>
      <td>{{$row->price}}</td>
      <td>{{$row->qty}}</td>
      <td>{{$row->total}}</td>
      <td><center><a href="{{ route('remove', ['row' => $rowid ]) }}" class="fas fa-trash"></a></center></td>
    </tr>
    @endforeach
  </tbody>
</table>  
<table class="table table-bordered table-hover">
  <thead class="">
    <tr>
      <th scope="col">Cantidad articulos</th>
      <th scope="col">Total compra $</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>{{Cart::count()}}</th>
      <td>{{Cart::total()}}</td>
    </tr>
  </tbody>
</table>  
<h3 class="col-sm-12">Datos de envío</h3>   
        {!! Form::open([ 'method' => 'POST','class' => 'row', 'url' => 'https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu']) !!}
        {{-- https://checkout.payulatam.com/ppp-web-gateway-payu
        <input name="merchantId"    type="hidden"  value="723679" >
        <input name="accountId"     type="hidden"  value="728601" > produccion--}}
        
        {{-- https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu
        <input name="merchantId"    type="hidden"  value="508029" >
        <input name="accountId"     type="hidden"  value="512322" > desarrollo--}}

        {{ csrf_field() }}
        	<?php $referenceCode = time();?>
        	<input name="merchantId"    type="hidden"  value="508029" >
            <input name="accountId"     type="hidden"  value="512322" >
            <input name="tax"           type="hidden"  value="{{Cart::tax()}}"  >
            <input name="taxReturnBase" type="hidden"  value="0" >
            <input name="currency"      type="hidden"  value="ARS" >
            <input name="buyerFullName"    type="hidden"  value="{!!Auth::user()->name.' '.Auth::user()->surname!!}" id="buyerFullName">
            <input name="mobilePhone"    type="hidden"  value="{!!Auth::user()->phone!!}" id="mobilePhone">
            <input name="amount"        type="hidden"  value="{{Cart::total()}}" id="amount">
            <input name="shippingAddress" type="hidden" value="" id="shippingAddress" required>
            <input name="shippingCity" type="hidden" value="" id="shippingCity" required>
            <input name="shippingCountry" type="hidden" value="AR" id="shippingCountry">
            <input name="zipCode"    type="hidden"  value="" id="zipCode">
            <input name="billingCity" type="hidden" value="" id="billingCity" required>
            <input name="description"   type="hidden"  value="{{$desc}}" id="description" >
            <input name="extra2"    type="hidden"  value="{{$extra2}}" id="extra2">
            <input name="extra3"    type="hidden"  value="{{$extra3}}" id="extra3"> 
            <input name="billingAddress" type="hidden" value="{{$extra3}}" id="billingAddress" required>
            <input name="responseUrl"    type="hidden"  value="http://yap.touchandshopapp.com/respuestatys.php" >
            <input name="confirmationUrl" type="hidden" value="http://yap.touchandshopapp.com/confirmaciontys.php">
            <input name="test"          type="hidden"  value="1" >            
            <input name="referenceCode" type="hidden"  value="{!!$referenceCode	!!}" id="referenceCode">
            <input name="signature"     type="hidden"  value="{!!md5('4Vj8eK4rloUd272L48hsrarnUA'.'~'.'508029'.'~'.$referenceCode.'~'.Cart::total().'~'.'ARS')!!}"  id="signature">  
            <input name="extra1"    type="hidden"  value="{!!md5(Auth::user()->id.'~'.Auth::user()->dni.'~'.Auth::user()->email).'|'.$place!!}" id="extra1">                                   

            <div class="form-group col-sm-4 {{ $errors->has('buyerEmail') ? ' has-error' : '' }}">
                {!! Form::label('buyerEmail','Email') !!}
                {!! Form::email('buyerEmail',Auth::user()->email,['class' => 'form-control','placeholder' => 'Email','required', 'value' => Auth::user()->email]) !!}
                @if ($errors->has('buyerEmail'))
                    <span class="help-block">
                        <strong>{{ $errors->first('buyerEmail') }}</strong>
                    </span>
                @endif
            </div>
            <div class="changeAddress_provincia form-group col-sm-4"></div>
        	<div class="changeAddress_localidad form-group col-sm-4"></div>
            <div class="form-group col-sm-4 {{ $errors->has('addressInput') ? ' has-error' : '' }}">
                {!! Form::label('addressInput','Dirección (Calle y número)') !!}
                {!! Form::text('addressInput',null,['class' => 'form-control','placeholder' => 'Dirección (Calle y número)','required', 'onchange' => 'short()']) !!}
            </div>
            <div class="form-group col-sm-4">
	           	<label>Método de envío	</label>
		        <select id="envio" onchange="short();" class="form-control">
		          	<option value="">Seleccionar</option>
		           	<option value="Moto">Moto</option>
		           	<option value="Via CARGO">Via CARGO</option>
		        </select>
        	</div>
            <div class="col-sm-12">
	            <div class="row justify-content-md-center">
		            <div class="form-group col-sm-auto">
		                {!! Form::submit('Hacé tu pedido',['class' => 'btn palette-pumpkin']) !!}

		            </div>
	            </div>
        	</div>
        	<p>
        		<center><span id="buyNote1" style="color:red;" class="col-sm-12">* Complete los campos de dirección y envío</span></center>
        	</p>
        	@if(intval(Cart::total())<1500)
        	<p>	
        		<center><span id="buyNote2" style="color:red;" class="col-sm-12">* Compra mínima $1500</span></center>
        	</p>
        	@endif
        	
        	
        {!! Form::close() !!}
 	</div>
</div>
@endsection
<script type="text/javascript" src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/checkout.js')}}"></script>