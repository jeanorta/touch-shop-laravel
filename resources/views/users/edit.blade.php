@extends('layouts.main')
@section('title','Perfil')
@section('content')
	<div class="container">
		@include('flash::message')
		<div class="w3-panel w3-card-4 w3-white w3-opacity-min2">
        <h2 class="col-sm-12">Editar mi perfil<hr/></h2>      
        {!! Form::open([ 'method' => 'PUT','class' => 'row', 'route' => array('users.update', Auth::user()->id)]) !!}
        {{ csrf_field() }}
            <div class="form-group col-sm-4 {{ $errors->has('name') ? ' has-error' : '' }}">
                {!! Form::label('name','Nombre') !!}
                {!! Form::text('name',Auth::user()->name,['class' => 'form-control','placeholder' => 'Nombre','required','value' => Auth::user()->name]) !!}
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-sm-4 {{ $errors->has('surname') ? ' has-error' : '' }}">
                {!! Form::label('surname','Apellido') !!}
                {!! Form::text('surname',Auth::user()->surname,['class' => 'form-control','placeholder' => 'Apellido','required','value' => Auth::user()->surname]) !!}
                @if ($errors->has('surname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('surname') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-sm-4 {{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::label('email','Email') !!}
                {!! Form::email('email',Auth::user()->email,['class' => 'form-control','placeholder' => 'Email','required','value' => Auth::user()->email]) !!}
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-sm-4 {{ $errors->has('phone') ? ' has-error' : '' }}">
                {!! Form::label('phone','Teléfono') !!}
                {!! Form::text('phone',Auth::user()->phone,['class' => 'form-control','placeholder' => 'Teléfono','required','value' => Auth::user()->phone]) !!}
                @if ($errors->has('phone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-sm-4 {{ $errors->has('dni') ? ' has-error' : '' }}">
                {!! Form::label('dni','DNI') !!}
                {!! Form::text('dni',Auth::user()->dni,['class' => 'form-control','placeholder' => 'DNI','required','value' => Auth::user()->dni]) !!}
                @if ($errors->has('dni'))
                    <span class="help-block">
                        <strong>{{ $errors->first('dni') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-sm-4 {{ $errors->has('cuil') ? ' has-error' : '' }}">
                {!! Form::label('cuil','CUIL') !!}
                {!! Form::text('cuil',Auth::user()->cuil,['class' => 'form-control','placeholder' => 'CUIL','required','value' => Auth::user()->cuil]) !!}
                @if ($errors->has('cuil'))
                    <span class="help-block">
                        <strong>{{ $errors->first('cuil') }}</strong>
                    </span>
                @endif
            </div>
            <div class="changeAddress1 form-group col-sm-4"></div>
            <div class="changeAddress2 form-group col-sm-4"></div>
            <div class="form-group col-sm-4 {{ $errors->has('address') ? ' has-error' : '' }}">
                {!! Form::label('address','Dirección (Calle y número)') !!}
                {!! Form::text('address',Auth::user()->address,['class' => 'form-control','placeholder' => 'Dirección (Calle y número)','required','onchange' => 'short()']) !!}
            </div>
            {!! Form::hidden('city',Auth::user()->city,['id' => 'city']) !!}
            {!! Form::hidden('state',Auth::user()->state,['id' => 'state']) !!}
            {!! Form::hidden('postalCode',Auth::user()->postalCode,['id' => 'postalCode']) !!}
            {!! Form::hidden('country','AR') !!}
            <div class="form-group col-sm-12">
                {!! Form::submit('Actualizar',['class' => 'btn palette-pumpkin']) !!}
            </div>
                
        {!! Form::close() !!}
    </div>
</div>
<script type="text/javascript" src="{{asset('js/Usercreate.js')}}"></script>
 	</div> 
@endsection