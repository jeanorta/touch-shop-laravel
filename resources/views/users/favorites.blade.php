@extends('layouts.main')
@section('title','Favoritos')
@section('content')
  <div class="container">
    <div class="row"> 
      <div class="col-sm-6 w3-panel w3-white">
        <div class="row w3-container">
          <div class="col-sm-12 w3-panel" style="margin-top: 20px;"><h6> {{'Productos Favoritos: '.count($likeUserProduct->toArray()).' resultados'}}</h6></div>
        </div>
        <hr style="margin-top: 0px;"> 
        <div class="w3-container">
          {{-- catalogo de Productos --}}          
          <div class="row" id="catalog">            
            @foreach($products as $product)
              <?php  
                $product2 = explode("|",$product->imageProduct ); 
                if ($product2[0]===""){
                  $product2[0] = "img/noImageDirectory.png";
                }else{
                  $product2[0] = env('APP_CONTEXT_FOLDER_LOCAL').$product2[0];
                }
              ?>
              @foreach($likeUserProduct as $like) 
              @if($product -> idProduct == $like -> idProduct)            
              <div class="col-sm-6">
                <a href="{{ route('products.details', ['id' => $product -> idProduct]) }}"> 
                <div class="row"><div class="col-sm-11 w3-panel w3-card-2 w3-hover-shadow" style="height: 330px;"><center><div style="min-height: 200px;"><img src="{{$product2[0]}}" class="img-catalg img-responsive" style="max-width: 100%; height: auto !important;"></div></center><span style="font-size: 11; font-weight: bold;">{{ $product -> nameProduct}}</span><br><div style="font-size: 10px !important; color: #858585; height: 70px;">{{ $product -> descProduct}}</div><b>$ {{ $product -> majorPriceProduct}}</b><a href="{{route('users.favorites',['id' => $product -> idProduct])}}" class="w3-right"><i class="fa fa-heart"></i></a></div></div>
                </a>
              </div>
              @endif
              @endforeach
            @endforeach            
          </div>{{-- fin catalogo --}}
        </div>
      </div>           
      <div class="col-sm-6 w3-panel w3-white">
        <div class="row w3-container">
          <div class="col-sm-12 w3-panel" style="margin-top: 20px;"><h6> {{'Locales Favoritos: '.count($likeUserPlace->toArray()).' resultados'}}</h6></div>
        </div>
        <hr style="margin-top: 0px;"> 
        <div class="w3-container">          
          {{-- directorio de locales --}}            
          <div class="row" id="directory">            
            @foreach($places as $place)
              <?php  
                $place2 = explode("|",$place->imagePlace ); 
                if ($place2[0]===""){
                  $place2[0] = "img/noImageDirectory.png";
                }else{
                  $place2[0] = env('APP_CONTEXT_FOLDER').$place2[0];
                }
              ?>
              @foreach($likeUserPlace as $like) 
              @if($place -> idPlace == $like -> idPlace)             
              <div class="col-sm-6">
                <a href="{{ route('places.details', ['id' => $place -> idPlace]) }}"> 
                  <div class="row"><div class="col-sm-11 w3-panel w3-card-2 w3-hover-shadow" style="height: 330px;"><center><div style="min-height: 200px;"><img src="{{$place2[0]}}" class="img-catalg img-responsive"></div></center><span style="font-size: 11; font-weight: bold;">{{ $place -> namePlace}}</span><br><div style="font-size: 10px !important; color: #858585; height: 70px;">{{ $place -> addressPlace}}<br><strong>mail: </strong> {{ $place -> emailPlace}}<br><strong>Telf.: </strong> {{ $place -> phonePlace}}</div><a href="{{route('users.favorites',['place' => $place -> idPlace])}}" class="w3-right"><i class="fa fa-heart"></i></a></div></div>
                </a>
              </div>
              @endif
              @endforeach
            @endforeach           
          </div>
          {{-- fin directorio --}}
        </div>
      </div>          
    </div>
  </div>  
  <!-- End Page Content -->
@endsection