{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}

@extends('layouts.main')
@section('title','Crear Usuario')
@section('content')
<div class="container">
    <div class="w3-panel w3-card-4 w3-white w3-opacity-min2">
        <h2 class="col-sm-12">Crear Usuario <hr/></h2>      
        {!! Form::open([ 'method' => 'POST','class' => 'row', 'route' => 'register']) !!}
        {{ csrf_field() }}
            <div class="form-group col-sm-4 {{ $errors->has('name') ? ' has-error' : '' }}">
                {!! Form::label('name','Nombre') !!}
                {!! Form::text('name',null,['class' => 'form-control','placeholder' => 'Nombre','required']) !!}
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-sm-4 {{ $errors->has('surname') ? ' has-error' : '' }}">
                {!! Form::label('surname','Apellido') !!}
                {!! Form::text('surname',null,['class' => 'form-control','placeholder' => 'Apellido','required']) !!}
                @if ($errors->has('surname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('surname') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-sm-4 {{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::label('email','Email') !!}
                {!! Form::email('email',null,['class' => 'form-control','placeholder' => 'Email','required']) !!}
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-sm-4 {{ $errors->has('phone') ? ' has-error' : '' }}">
                {!! Form::label('phone','Teléfono') !!}
                {!! Form::text('phone',null,['class' => 'form-control','placeholder' => 'Teléfono','required']) !!}
                @if ($errors->has('phone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-sm-4 {{ $errors->has('dni') ? ' has-error' : '' }}">
                {!! Form::label('dni','DNI') !!}
                {!! Form::text('dni',null,['class' => 'form-control','placeholder' => 'DNI','required']) !!}
                @if ($errors->has('dni'))
                    <span class="help-block">
                        <strong>{{ $errors->first('dni') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-sm-4 {{ $errors->has('cuil') ? ' has-error' : '' }}">
                {!! Form::label('cuil','CUIL') !!}
                {!! Form::text('cuil',null,['class' => 'form-control','placeholder' => 'CUIL','required']) !!}
                @if ($errors->has('cuil'))
                    <span class="help-block">
                        <strong>{{ $errors->first('cuil') }}</strong>
                    </span>
                @endif
            </div>
            <div class="changeAddress1 form-group col-sm-4"></div>
            <div class="changeAddress2 form-group col-sm-4"></div>
            <div class="form-group col-sm-4 {{ $errors->has('address') ? ' has-error' : '' }}">
                {!! Form::label('address','Dirección (Calle y número)') !!}
                {!! Form::text('address',null,['class' => 'form-control','placeholder' => 'Dirección (Calle y número)','required']) !!}
            </div>
            <div class="form-group col-sm-4 {{ $errors->has('password') ? ' has-error' : '' }}">
                {!! Form::label('password','Contraseña') !!}
                <input type="password" name="password" class="form-control">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-sm-4">
                <label for="password-confirm">Confirm Password</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            </div>
            {!! Form::hidden('city',null,['id' => 'city']) !!}
            {!! Form::hidden('state',null,['id' => 'state']) !!}
            {!! Form::hidden('postalCode',null,['id' => 'postalCode']) !!}
            {!! Form::hidden('country','AR') !!}
            <div class="form-group col-sm-12">
                {!! Form::submit('Registrar',['class' => 'btn palette-pumpkin']) !!}
            </div>
                
        {!! Form::close() !!}
    </div>
</div>
<script type="text/javascript" src="{{asset('js/Usercreate.js')}}"></script>
@endsection
