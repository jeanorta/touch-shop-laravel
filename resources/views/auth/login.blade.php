{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}

@extends('layouts.main')
@section('title','Login')
@section('content')
<div class="container">
    <div class="w3-panel w3-card-4 w3-white w3-opacity-min2">
        <div class="row">
            <h2 class="col-sm-9">Iniciar Sesión</h2>
            <h2 class="col-sm-3 text-right btn-group" style="justify-content: flex-end;">
                <a href="{!!route('register')!!}" class="btn btn-warning" >Registrarse</a>
            </h2>                       
        </div>
        <hr/>       
        {!! Form::open([ 'method' => 'POST','class' => 'row', 'route' => 'login']) !!}
        {{ csrf_field() }}
            <div class="form-group col-sm-6 {{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::label('email','Email') !!}
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-sm-6 {{ $errors->has('password') ? ' has-error' : '' }}">
                {!! Form::label('password','Contraseña') !!}
                <input type="password" name="password" class="form-control" placeholder="********">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-sm-12">
                {!! Form::submit('Iniciar Sesión',['class' => 'btn palette-pumpkin']) !!}
            </div>
                
        {!! Form::close() !!}
    </div>
</div>
@endsection
