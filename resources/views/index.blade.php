@extends('layouts.main')
@section('title','Inicio')
@section('content')
<link href="theme/css/style.css" rel="stylesheet" type="text/css" media="all" />
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
	<head>
		<script type="text/javascript">
	        $(document).ready(function() {
	            $(".dropdown img.flag").addClass("flagvisibility");

	            $(".dropdown dt a").click(function() {
	                $(".dropdown dd ul").toggle();
	            });
	                        
	            $(".dropdown dd ul li a").click(function() {
	                var text = $(this).html();
	                $(".dropdown dt a span").html(text);
	                $(".dropdown dd ul").hide();
	                $("#result").html("Selected value is: " + getSelectedValue("sample"));
	            });
	                        
	            function getSelectedValue(id) {
	                return $("#" + id).find("dt a span.value").html();
	            }

	            $(document).bind('click', function(e) {
	                var $clicked = $(e.target);
	                if (! $clicked.parents().hasClass("dropdown"))
	                    $(".dropdown dd ul").hide();
	            });


	            $("#flagSwitcher").click(function() {
	                $(".dropdown img.flag").toggleClass("flagvisibility");
	            });
	        });
	    </script>
		<!-- top scrolling -->
	    <script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
				});
			});
		</script>
	</head>
  	<div class="w3-white">
       	<div class="index-banner">
       	  <div class="wmuSlider example1" style="height: 560px;">
			  <div class="wmuSliderWrapper">
				  <article style="position: relative; width: 100%; opacity: 1;"> 
				   	 <div class="banner-wrap">
					   	<div class="slider-left">
							<img src="theme/images/banner1.jpg" alt=""/> 
						</div>
						 <div class="slider-right">
						    <h1>Mujer</h1>
						    <h2>Moda</h2>
						    <p>Todo lo que ellas necesitan</p>
						    
						 </div>
						 <div class="clear"></div>
					 </div>
				   </article>
				   <article style="position: absolute; width: 100%; opacity: 0;"> 
				   	 <div class="banner-wrap">
					   	<div class="slider-left">
							<img src="theme/images/banner2.jpg" alt=""/> 
						</div>
						 <div class="slider-right">
						    <h1>Hombre</h1>
						    <h2>Elegancia</h2>
						    <p>Encuentra tu look perfecto</p>
						    
						 </div>
						 <div class="clear"></div>
					 </div>
				   </article>
				   <article style="position: absolute; width: 100%; opacity: 0;">
				   	 <div class="banner-wrap">
					   	<div class="slider-left">
							<img src="theme/images/banner3.jpg" alt=""/> 
						</div>
						 <div class="slider-right">
						    <h1>Niños</h1>
						    <h2>Variedad</h2>
						    <p>Variedad en diseños</p>
						    
						 </div>
						 <div class="clear"></div>
					 </div>
				   </article>
				   <article style="position: absolute; width: 100%; opacity: 0;">
					   	<div class="banner-wrap">
						   	<div class="slider-left">
								<img src="theme/images/banner4.jpg" alt=""/> 
							</div>
							 <div class="slider-right">
							    <h1>Deporte</h1>
							    <h2>Clásico</h2>
							    <p>Deportes y entrenamiento</p>
							    
							 </div>
							 <div class="clear"></div>
						 </div>
				   </article>
				   <article style="position: absolute; width: 100%; opacity: 0;"> 
				   	 <div class="banner-wrap">
					   	<div class="slider-left">
							<img src="theme/images/banner5.jpg" alt=""/> 
						</div>
						 <div class="slider-right">
						    <h1>Descargá</h1>
						    <h2>App T&S</h2>
						    <p>LLevá en tu bolsillo en todo momento</p>
						    
						 </div>
						 <div class="clear"></div>
					 </div>
			      </article>
				</div>
                <a class="wmuSliderPrev">Previous</a><a class="wmuSliderNext">Next</a>
                <!--<ul class="wmuSliderPagination">
                	<li><a href="#" class="">0</a></li>
                	<li><a href="#" class="">1</a></li>
                	<li><a href="#" class="wmuActive">2</a></li>
                	<li><a href="#">3</a></li>
                	<li><a href="#">4</a></li>
                  </ul>-->
                 <a class="wmuSliderPrev">Previous</a><a class="wmuSliderNext">Next</a>
                 <!--<ul class="wmuSliderPagination" >
                 	<li><a href="#" class="wmuActive">0</a></li>
                 	<li><a href="#" class="">1</a></li>
                 	<li><a href="#" class="">2</a></li>
                 	<li><a href="#" class="">3</a></li>
                 	<li><a href="#" class="">4</a></li>
                 </ul>-->
          </div>
            	 <script src="theme/js/jquery.wmuSlider.js"></script> 
				 <script type="text/javascript" src="theme/js/modernizr.custom.min.js"></script> 
						<script>
       						 $('.example1').wmuSlider();         
   						</script> 	           	      
        </div>
    <div class="container">
    	<center><h1 class="w3-padding-32">PRODUCTOS DESTACADOS</h1></center>
    <div class="row" id="catalog">
        @foreach($products as $product)
            <?php  
              $product2 = explode("|",$product->imageProduct ); 
              if ($product2[0]===""){
                $product2[0] = "img/noImageDirectory.png";
              }else{
                $product2[0] = env('APP_CONTEXT_FOLDER').$product2[0];
              }
            ?>
             
            <div class="col-sm-3">
                <a href="{{ route('products.details', ['id' => $product -> idProduct]) }}"> 
                <div class="row"><div class="col-sm-11 w3-panel w3-card-2 w3-hover-shadow" style="height: 330px;"><center><div style="min-height: 200px;"><img src="{{$product2[0]}}" class="img-catalg img-responsive" style="max-width: 100%; height: auto !important;"></div></center><span style="font-size: 11; font-weight: bold;">{{ $product -> nameProduct}}</span><br><div style="font-size: 10px; color: #858585; height: 70px;">{{ $product -> descProduct}}</div><b>$ {{ $product -> majorPriceProduct}}</b><a href="{{route('users.favorites',['id' => $product -> idProduct])}}" class="w3-right">
                	<?php $flatProduct = true;?>
                    @foreach($likeUserProduct as $like)                     
                    @if($product -> idProduct == $like -> idProduct)
                    <i class="fa fa-heart"></i>
                    <?php $flatProduct = false;?>                      
                    @endif
                    @endforeach
                    @if($flatProduct)
                    <i class="fa fa-heart-o"></i>
                    @endif</a></div></div>
                </a>
              </div>
        @endforeach
    </div>
    <div class="w3-padding-32">
    	<center><h1 class="">LOCALES RECONOCIDOS</h1></center>
    </div>
    <div class="row" id="directory">            
            @foreach($places as $place)
            <?php  
              $place2 = explode("|",$place->imagePlace ); 
              if ($place2[0]===""){
                $place2[0] = "img/noImageDirectory.png";
              }else{
                $place2[0] = env('APP_CONTEXT_FOLDER').$place2[0];
              }
            ?>
             
            <div class="col-sm-3">
                <a href="{{ route('places.details', ['id' => $place -> idPlace]) }}"> 
                  <div class="row">
                  	<div class="col-sm-11 w3-panel w3-card-2 w3-hover-shadow" style="height: 330px;"><center><div style="min-height: 200px;"><img src="{{$place2[0]}}" class="img-catalg img-responsive"></div></center><span style="font-size: 11; font-weight: bold;">{{ $place -> namePlace}}</span><br><div style="font-size: 10px; color: #858585; height: 70px;">{{ $place -> addressPlace}}<br><strong>mail: </strong> {{ $place -> emailPlace}}<br>
                  	<strong>Telf.: </strong> {{ $place -> phonePlace}}</div>
                  	<a href="{{route('users.favorites',['place' => $place -> idPlace])}}" class="w3-right">
                  		<?php $flatPlace = true;?>
	                    @foreach($likeUserPlace as $like)                     
	                    @if($place -> idPlace == $like -> idPlace)
	                    <i class="fa fa-heart"></i>
	                    <?php $flatPlace = false;?>                      
	                    @endif
	                    @endforeach
	                    @if($flatPlace)
	                    <i class="fa fa-heart-o"></i>
	                    @endif
                  	</a>
                  </div>
              </div>
                </a>
              </div>	
            @endforeach            
            </div>
	</div>
	<!-- The about Section -->
	<div class="w3-container w3-content w3-center w3-padding-32 w3-white" style="max-width:800px" id="band">
    <h2 class="w3-wide">Touch And Shop</h2>
    <p class="w3-opacity"><i>Porque amamos las comprar igual que vos</i></p>
    <p class="w3-justify">Te ofrecemos la más exclusiva guía de compras para los locales de las zonas más demandadas de la Av. Avellaneda y la Salada, donde podrás recorrer cada una de las tiendas, conocer sus catálogos, comunicarte e incluso comprar en linea sin salir de casa, y para los más aventureros te ofrecemos nuestra App, para que puedas ubicar cada una de estas tiendas y hacer tus compras de forma precesencial habiendo conocido los precios de antemano.</p>
    <div class="w3-row w3-padding-32">
      <div class="w3-half">
        <p>Descargá para Iphone</p>
        <a href="https://itunes.apple.com/us/app/touch-and-shop-avellaneda/id1320291861?l=es&ls=1&mt=8">
        	<img src="theme/images/iphone.png" class="w3-round w3-margin-bottom" alt="Random Name" style="width:256px; height: 256px;">
    	</a>
      </div>

      <div class="w3-half">
        <p>Descargá para Android</p>
        <a href="https://play.google.com/store/apps/details?id=py.v2TYS">
        	<img src="theme/images/android.png" class="w3-round" alt="Random Name" style="width:256px; height: 256px;">
    	</a>
      </div>
    </div>
  </div>

  <!-- The Contact Section -->
  <div class="w3-container w3-content w3-padding-64  w3-opacity-min2" style="max-width:800px; margin-bottom:16px;" id="contact">
    <h2 class="w3-wide w3-center">CONTACTO</h2>
    {{-- <p class="w3-opacity w3-center"><i>Fan? Drop a note!</i></p> --}}
    <div class="w3-row w3-padding-32">
      <div class="w3-col m6 w3-large w3-margin-bottom">
        <i class="fa fa-map-marker" style="width:30px"></i> CABA, BA<br>
        {{-- <i class="fa fa-phone" style="width:30px"></i> Phone: +00 151515<br> --}}
        <i class="fa fa-envelope" style="width:30px"> </i> Email: contacto@touchandshopapp.com<br>
      </div>
      <div class="w3-col m6">
        <form action="/action_page.php" target="_blank">
          <div class="w3-row-padding" style="margin:0 -16px 8px -16px">
            <div class="w3-half">
              <input class="w3-input w3-border" type="text" placeholder="Name" required name="Name">
            </div>
            <div class="w3-half">
              <input class="w3-input w3-border" type="text" placeholder="Email" required name="Email">
            </div>
          </div>
          <input class="w3-input w3-border" type="text" placeholder="Message" required name="Message">
          <button class="w3-button w3-black w3-section w3-right" type="submit">ENVIAR</button>
        </form>
      </div>
    </div>
  </div>

    </div>    
       <script type="text/javascript">
			$(document).ready(function() {
			
				var defaults = {
		  			containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
		 		};
				
				
				$().UItoTop({ easingType: 'easeOutQuart' });
				
			});
		</script>
    <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
@endsection

<script type="text/javascript" src="{{asset('js/index.js')}}"></script>
