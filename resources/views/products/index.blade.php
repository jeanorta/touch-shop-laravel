
@extends('layouts.main')
@section('title','Productos')
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-sm-3 " >
        <div class="row">
          <!-- The Filter Section -->
          <div class="col-sm-11 w3-panel" > 
            {!! Form::open([ 'method' => 'GET','class' => 'row', 'route' => 'products.index']) !!}
            <input class="form-control col-sm-10" id="myInput" name="search"  type="text" placeholder="Buscar.."> 
            <button class="btn palette-pumpkin col-sm-2"><i class="fas fa-search"></i></button>
            {!! Form::close()!!}  
          </div>
          <div class="col-sm-11 w3-panel w3-card-4 w3-white w3-bar-block" style="margin-top: 0px;"> 
            <?php
              $rubroBase = "";
            ?>
            @foreach($rubros as $rubro)
              @if($rubro->idRubro===$idRequest)
                <?php
                  $rubroBase = $rubro -> descRubro;
                ?>
              @endif
            @endforeach 
            {{$rubroBase.' '.$products->Total() .' resultados'}}<hr>    
                  <?php
                    $array2 = [];
                    $array2[0] = null; 
                  ?>  
                  @foreach($rubros as $rubro)                
                    <?php
                    $id = 1;
                    $id = $idRequest;
                    $idRubro = $rubro -> idRubro;
                    ?>
                    @if($id === $idRubro)
                      <?php                 
                        $codigo = $rubro -> iconRubro;
                        $array2 = explode(".", $codigo);
                      ?>
                    
                    @endif
                  @endforeach                               
                  @foreach($rubros as $rubro)
                    <?php 
                      $idRubro = $rubro -> idRubro;                
                      $codigo = $rubro -> iconRubro;
                      $array = explode(".", $codigo);
                    ?>                   
                    @if($array[0]===$array2[0])
                      @if($array[1]==='000' && $array[2]==='000' && ($array[0] < 5))
                        @if($id===$idRubro)
                          <h6  style="font-size: 13px; color: red;"><span class="fas fa-caret-right"></span><strong> {{ $rubro -> descRubro}}</strong></h6>
                        @else
                          <h6  style="font-size: 13px;"><strong>{{ $rubro -> descRubro}}</strong></h6>
                        @endif
                      @endif

                      @if($array[1] !== '000' && $array[2]==='000' && ($array[0] < 5))                  
                        @if($id===$idRubro)
                          <a href="#" class="w3-bar-item item2 " style="color: red;"><span class="fas fa-caret-right"></span><strong>{{ $rubro -> descRubro}}</strong></a>
                        @else
                          <a href="{{ route('products.index', ['id' => $rubro -> idRubro]) }}" class="w3-bar-item item2 "><strong>{{ $rubro -> descRubro}}</strong></a>
                        @endif
                      @endif

                      @if($array[1] !== '000' && $array[2]!=='000' && ($array[0] < 5))                  
                        @if($id===$idRubro)
                          <a href="#" class="w3-bar-item item2 tab1" style="color: red;"><span class="fas fa-caret-right"></span> {{ $rubro -> descRubro}}</a>
                        @else
                          <a href="{{ route('products.index', ['id' => $rubro -> idRubro]) }}" class="w3-bar-item item2 tab1">{{ $rubro -> descRubro}}</a>
                        @endif
                      @endif 
                    @endif
                  @endforeach               
          </div>
        </div>
      </div>
      <div class="col-sm-9 w3-panel w3-card-4 w3-white">
        <div class="w3-container">
          {{-- catalogo de Productos --}}          
          <h6>{{$rubroBase.' '.$products->Total() .' resultados'}}</h6><hr>
          <div class="row" id="catalog">            
            @foreach($products as $product)
              <?php  
                $product2 = explode("|",$product->imageProduct ); 
                if ($product2[0]===""){
                  $product2[0] = "img/noImageDirectory.png";
                }else{
                  $product2[0] = env('APP_CONTEXT_FOLDER_LOCAL').$product2[0];
                }
              ?>             
              <div class="col-sm-3">
                <a href="{{ route('products.details', ['id' => $product -> idProduct]) }}"> 
                <div class="row"><div class="col-sm-11 w3-panel w3-card-2 w3-hover-shadow" style="height: 330px;"><center><div style="min-height: 200px;"><img src="{{$product2[0]}}" class="img-catalg img-responsive" style="max-width: 100%; height: auto !important;"></div></center><span style="font-size: 11; font-weight: bold;">{{ $product -> nameProduct}}</span><br><div style="font-size: 10px; color: #858585; height: 70px;">{{ $product -> descProduct}}</div><b>$ {{ $product -> majorPriceProduct}}</b><a href="{{route('users.favorites',['id' => $product -> idProduct])}}" class="w3-right">
                  <?php $flatProduct = true;?>
                    @foreach($likeUserProduct as $like)                     
                    @if($product -> idProduct == $like -> idProduct)
                    <i class="fa fa-heart"></i>
                    <?php $flatProduct = false;?>                      
                    @endif
                    @endforeach
                    @if($flatProduct)
                    <i class="fa fa-heart-o"></i>
                    @endif
                </a></div></div>
                </a>
              </div>
            @endforeach
            <div id=paginationCatalog class="col-sm-12">
              {!! $products->appends(['id' => $idRequest])->render() !!} 
            </div>            
          </div>{{-- fin catalogo --}}
        </div>
      </div>
    </div>      
              
  </div>
  <!-- The Contact Section -->
  <div class="row w3-container w3-content w3-padding-64 w3-black w3-opacity-min2" style="max-width:800px; margin-bottom:16px;" id="contact">
    <h2 class="w3-wide w3-center">CONTACTO</h2>
    {{-- <p class="w3-opacity w3-center"><i>Fan? Drop a note!</i></p> --}}
    <div class="w3-row w3-padding-32">
      <div class="w3-col m6 w3-large w3-margin-bottom">
        <i class="fa fa-map-marker" style="width:30px"></i> CABA, BA<br>
        {{-- <i class="fa fa-phone" style="width:30px"></i> Phone: +00 151515<br> --}}
        <i class="fa fa-envelope" style="width:30px"> </i> Email: mail@mail.com<br>
      </div>
      <div class="w3-col m6">
        <form action="/action_page.php" target="_blank">
          <div class="w3-row-padding" style="margin:0 -16px 8px -16px">
            <div class="w3-half">
              <input class="w3-input w3-border" type="text" placeholder="Name" required name="Name">
            </div>
            <div class="w3-half">
              <input class="w3-input w3-border" type="text" placeholder="Email" required name="Email">
            </div>
          </div>
          <input class="w3-input w3-border" type="text" placeholder="Message" required name="Message">
          <button class="w3-button w3-white w3-section w3-right" type="submit">ENVIAR</button>
        </form>
      </div>
    </div>
  </div>
@endsection
<script type="text/javascript" src="{{asset('js/index.js')}}"></script>