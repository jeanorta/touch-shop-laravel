@extends('layouts.main')
@section('title','Listar Productos')
@section('content')
	<div class="container">
		@include('flash::message')
	    <div class="w3-panel w3-card-4 w3-white">
	    	<div class="row">
            	<h2 class="col-sm-9">Listar artículos</h2>
	            <h2 class="col-sm-3 text-right btn-group" style="justify-content: flex-end;">
	                <a href="{{ route('products.create') }}" class="btn btn-warning" >Nuevo Artículo</a>
	            </h2>                       
        	</div>	
        	<hr/> 
        	<table class="table table-bordered table-hover">
  				<thead class="thead-dark">
				    <tr>
				      <th scope="col">id</th>
				      <th scope="col">Codigo</th>
				      <th scope="col">Nombre</th>
				      <th scope="col">Descripción</th>
				      <th scope="col" style="min-width: 95px;">Precio $</th>
				      <th scope="col">Imagen</th>
				      <th scope="col">Local</th>
				      <th scope="col">Acciones</th>
				    </tr>
  				</thead>
  				<tbody>
				  	@foreach($products as $product)
				    <tr>
				      <th scope="row">{{$product -> idProduct}}</th>
				      <td>{{$product -> codeProduct}}</td>
				      <td>{{$product -> nameProduct}}</td>
				      <td>{{$product -> descProduct}}</td>
				      <td>{{$product -> majorPriceProduct}}</td>
				      <td>{{$product -> imageProduct}}</td>
				      <td>{{$product -> idPlace}}</td>
				      <td><center><a href="{{ route('products.edit', ['id' => $product -> idProduct ]) }}" class="fas fa-edit" style="font-size: 20px;"></a> <a href="{{ route('products.delete', ['id' => $product -> idProduct ]) }}" class="fas fa-trash" style="font-size: 20px;"></a></center></td>
				    </tr>
				    @endforeach
  				</tbody>
			</table>
			<div id=paginationCatalog class="col-sm-12">
              {!! $products->render() !!} 
            </div>
	    </div>	    
	</div>
@endsection