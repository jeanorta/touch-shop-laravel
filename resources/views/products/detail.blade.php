@extends('layouts.main')
@section('title','Detalle Producto')
@section('content')

  <link href="theme/css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="theme/css/form.css" rel="stylesheet" type="text/css" media="all" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>


<!--details-product-slider-->
        <!-- Include the Etalage files -->
          <link rel="stylesheet" href="theme/css/etalage.css">
          <script src="theme/js/jquery.etalage.min.js"></script>
        <!-- Include the Etalage files -->
        <script>
            jQuery(document).ready(function($){
      
              $('#etalage').etalage({
                //thumb_image_width: 300,
                thumb_image_height: 300,
                
                show_hint: true,
                click_callback: function(image_anchor, instance_id){
                  alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
                }
              });
              // This is for the dropdown list example:
              $('.dropdownlist').change(function(){
                etalage_show( $(this).find('option:selected').attr('class') );
              });

          });
        </script>
<script type="text/javascript" src="theme/js/move-top.js"></script>
<script type="text/javascript" src="theme/js/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
      $(".scroll").click(function(event){   
        event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
      });
    });
</script> 
 
	  

 
   
       <div class="col-sm-12 w3-panel w3-card-4 w3-white">
        @foreach($products as $product)
        @if($product -> idProduct === $idRequest)
        <?php  
              $img = substr( $product->imageProduct , 0 , -1);
              $product2 = explode("|", $img); 
              if ($product2[0]===""){
                $product2[0] = "../noImageDirectory.png";
              }else{
                //$product2[0] = env('APP_CONTEXT_FOLDER_LOCAL').$product2[0];
              }
            ?>
         <div class="wrap">
          {{-- <div class="rsidebar span_1_of_left">
            <section  class="sky-form">
          <h4>Colores</h4>
            <div class="row row1 scroll-pane">
              <div class="col col-4">
                <label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>Red</label>
              </div>
              <div class="col col-4">
                <label class="checkbox"><input type="checkbox" name="checkbox"><i></i></label>
                <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Black</label>
                <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Yellow</label>
                <label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Orange</label>
              </div>
            </div>
            </section>
    </div> --}}
    <div class="cont span_2_of_3">
        <div class="labout span_1_of_a1">
        <!-- start product_slider -->
             <ul id="etalage">
              @foreach($product2 as $image)
              <li>
                <img class="etalage_thumb_image" src="{{env('APP_CONTEXT_FOLDER_LOCAL').$image}}" />
                <img class="etalage_source_image" src="{{env('APP_CONTEXT_FOLDER_LOCAL').$image}}" />
              </li>
              @endforeach
            </ul>
          
          
      <!-- end product_slider -->
      </div>
      <div class="cont1 span_2_of_a1">
        <h3 class="m_3">{{ $product -> nameProduct}}</h3>
        <p class="m_desc">{{ $product -> descProduct}}</p>
        <div class="price_single">
                {{-- <span class="reducedfrom">$66.00</span> --}}
                <span class="actual">${{ $product -> majorPriceProduct}}</span>{{-- <a href="#">click for offer</a> --}}
              </div>
        
        <div class="btn_form">
          {!! Form::open([ 'method' => 'POST','class' => 'row', 'route' => 'checkouts.store']) !!}
          <input type="hidden" name="id" value="{{ $product -> idProduct}}">
          <div class="m_3">
            <div class="form-group col-sm-12">
              <h4 class="m_9">Seleccione un Color</h4>
              <select name="color" class="form-control">
                <?php                 
                  $codigo = $product -> idRubro;
                  $rubroProduct = explode(",", $codigo);
                ?>
                @foreach($rubros as $rubro)
                  <?php                 
                    $codigo = $rubro -> iconRubro;
                    $array = explode(".", $codigo);
                  ?>
                  @for($i=0;$i<count($rubroProduct);$i++)
                  @if($rubro -> idRubro == $rubroProduct[$i])
                  @if($array[1] !== '000' && $array[2]==='000' && $array[0] == 7)
                  <option value="{{ $rubro -> descRubro}}">{{ $rubro -> descRubro}}</option>
                  @endif
                  @endif
                  @endfor
                @endforeach
              </select>
            </div>
            <div class="form-group col-sm-12">
              <h4 class="m_9">Seleccione un Talle</h4>
              <select name="size" class="form-control">
                @foreach($rubros as $rubro)
                  <?php                 
                    $codigo = $rubro -> iconRubro;
                    $array = explode(".", $codigo);
                  ?>
                  @for($i=0;$i<count($rubroProduct);$i++)
                  @if($rubro -> idRubro == $rubroProduct[$i])
                  @if($array[1] !== '001' && $array[2]!='000' && $array[0] == 5)
                  <option value="{{ $rubro -> descRubro}}">{{ $rubro -> descRubro}}</option>
                  @endif
                  @endif
                  @endfor
                @endforeach
              </select>
            </div>
            <div class="form-group col-sm-12">
              <h4 class="m_9">Seleccione la Cantidad</h4>
              <input type="number" name="qty" value="1" class="form-control">
            </div>
            <input type="submit" value="Agregar al carro"   title="" class="add-car-product" style="margin-left: 15px;">
          </div>
          
          {!! Form::close() !!}
        </div>
        <h4 class="m_9"><a href="{{route('users.favorites',['id' => $product -> idProduct])}}">
          <?php $flatProduct = true;?>
                    @foreach($likeUserProduct as $like)                     
                    @if($product -> idProduct == $like -> idProduct)
                    <i class="fa fa-heart"> Quitar de Favoritos</i>
                    <?php $flatProduct = false;?>                      
                    @endif
                    @endforeach
                    @if($flatProduct)
                    <i class="fa fa-heart-o"> Agregar a Favoritos</i>
                    @endif</a></div></div></a></h4>
          
          
                <!--<div class="social_single"> 
           <ul> 
            <li class="fb"><a href="#"><span> </span></a></li>
            <li class="tw"><a href="#"><span> </span></a></li>
            <li class="g_plus"><a href="#"><span> </span></a></li>
            <li class="rss"><a href="#"><span> </span></a></li>   
           </ul>
          </div>-->
      </div>
      <div class="clear"></div>
     
     
      <script type="text/javascript">
     $(window).load(function() {
      $("#flexiselDemo1").flexisel();
      $("#flexiselDemo2").flexisel({
        enableResponsiveBreakpoints: true,
          responsiveBreakpoints: { 
            portrait: { 
              changePoint:480,
              visibleItems: 1
            }, 
            landscape: { 
              changePoint:640,
              visibleItems: 2
            },
            tablet: { 
              changePoint:768,
              visibleItems: 3
            }
          }
        });
    
      $("#flexiselDemo3").flexisel({
        visibleItems: 5,
        animationSpeed: 1000,
        autoPlay: true,
        autoPlaySpeed: 3000,        
        pauseOnHover: true,
        enableResponsiveBreakpoints: true,
          responsiveBreakpoints: { 
            portrait: { 
              changePoint:480,
              visibleItems: 1
            }, 
            landscape: { 
              changePoint:640,
              visibleItems: 2
            },
            tablet: { 
              changePoint:768,
              visibleItems: 3
            }
          }
        });
        
    });
  </script>
  <script type="text/javascript" src="theme/js/jquery.flexisel.js"></script>
   {{-- <div class="toogle">
      <h3 class="m_3">Detalles del Producto</h3>
      <p class="m_text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
     </div>         
   <div class="toogle">
      <h3 class="m_3">Product Reviews</h3>
      <p class="m_text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
     </div> --}}
     </div>
     <div class="clear"></div>
   </div>
      @endif
      @endforeach
     </div>
    
       <script type="text/javascript">
      $(document).ready(function() {
      
        var defaults = {
            containerID: 'toTop', // fading element id
          containerHoverID: 'toTopHover', // fading element hover id
          scrollSpeed: 1200,
          easingType: 'linear' 
        };
        
        
        $().UItoTop({ easingType: 'easeOutQuart' });
        
      });
    </script>
        <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>

@endsection
