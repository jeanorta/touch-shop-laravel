@extends('layouts.main')
@section('title','Editar Productos')
@section('content')
	<div class="container">
		@include('flash::message')
	    <div class="w3-panel w3-card-4 w3-white">
	        <h2 class="col-sm-12">Editar artículos<hr/></h2>
	        @foreach($products as $product)
	        @if($product -> idProduct == $id)
	        {!! Form::open([ 'method' => 'PUT','class' => 'row', 'route' => array('products.update', $id), 'files' => true]) !!}
	        	{{ csrf_field() }}
	            <div class="form-group col-sm-1 {{ $errors->has('name') ? ' has-error' : '' }}">
	                {!! Form::label('codeProduct','Código') !!}
	                {!! Form::text('codeProduct',$product->codeProduct,['class' => 'form-control','required']) !!}
	            </div>
	            <div class="form-group col-sm-2 {{ $errors->has('name') ? ' has-error' : '' }}">
	                {!! Form::label('nameProduct','Nombre') !!}
	                {!! Form::text('nameProduct',$product->nameProduct,['class' => 'form-control','required']) !!}
	            </div>
	            <div class="form-group col-sm-2 {{ $errors->has('name') ? ' has-error' : '' }}">
	                {!! Form::label('descProduct','Descripción') !!}
	                {!! Form::text('descProduct',$product->descProduct,['class' => 'form-control','required']) !!}
	            </div>
	            <div class="form-group col-sm-2 {{ $errors->has('name') ? ' has-error' : '' }}">
	                {!! Form::label('idPlace','Local') !!}
	                <?php	
	                		$temp = substr( $product->idPlace , 0 , -2);
	                		$art = explode('(',$temp); 
	                ?>
	                <select id="idPlace" name="idPlace" class="form-control" required>
	                	@foreach($places as $place)
	                	@if($art[1]==$place->idPlace)
	                		<option value="{{$place->namePlace.' ('.$place->idPlace.'),'}}" selected="true">{{$place->namePlace}}</option>
	                	@else
	                		<option value="{{$place->namePlace.' ('.$place->idPlace.'),'}}">{{$place->namePlace}}</option>
	                	@endif	                	
	                	@endforeach
	                </select>
	            </div>
	            <div class="form-group col-sm-1 {{ $errors->has('name') ? ' has-error' : '' }}">
	                {!! Form::label('majorPriceProduct','Precio') !!}
	                {!! Form::text('majorPriceProduct',$product->majorPriceProduct,['class' => 'form-control','required']) !!}
	            </div>
	                <!-- COMPONENT START -->
					<div class="form-group col-sm-4">
						{!! Form::label('imageProduct','Imagen ('.$product->imageProduct.')') !!}
						<input type="hidden" name="imageOld" value="{{$product->imageProduct}}">
						<div class="input-group input-file" >
							<span class="input-group-btn">
				        		<button class="btn btn-default btn-choose" type="button">Elegir</button>
				    		</span>
				    		<input type="text" class="form-control" placeholder='imagen jpg,gif o png' />
				    		<span class="input-group-btn">
				       			 <button class="btn btn-warning btn-reset" type="button">Reset</button>
				    		</span>
						</div>
					</div>
					<!-- COMPONENT END -->	        	
	        	<div class="form-group col-sm-2 {{ $errors->has('name') ? ' has-error' : '' }}">
	                {!! Form::label('rubro','Género: ') !!}<br>
	                <input class="M" type="checkbox" name="rubro[]" value="42,177">Masculino<br>
	                <input class="F" type="checkbox" name="rubro[]" value="1,176">Femenino<br>
	                <input class="N" type="checkbox" name="rubro[]" value="80,179">Niños<br>
	                <input class="N" type="checkbox" name="rubro[]" value="80,178">Niñas<br>
	                <input class="N" type="checkbox" name="rubro[]" value="80,175">Bebé<br>
	                <input class="U" type="checkbox" name="rubro[]" value="1,42,180">Unisex<br><br>
	                <input type="checkbox" name="rubro[]" value="93">Es deportivo?
	            </div>
	            @for($j=1; $j<=3;$j++)	{{-- Tipo --}}    
			            @foreach($rubros as $rubro)
				            <?php                 
					            $codigo = $rubro -> iconRubro;
					            $array = explode(".", $codigo);
					        ?>
					        @if($array[0] === '00'.$j && $array[1]==='000' && $array[2]==='000')
					        	<div class="form-group col-sm-2 rub {{'type'.$j}}"  style="display: none;">
				              	{!! Form::label('codeProduct',$rubro -> descRubro.': ') !!}<br>
				            @endif
				            @if($array[0] === '00'.$j && $array[1]!=='000' && $array[2]==='000')
				              	<input type="checkbox" name="rubro[]" value="{{ $rubro -> idRubro}}" class="{{$j.$rubro -> descRubro}}">{{ $rubro -> descRubro}}<br>
				            @endif		                
			            @endforeach
			            </div>
	            @endfor
	            @for($j=1; $j<=3;$j++)	{{-- Productos --}}
		            @for($i=1; $i<=3;$i++)    
			            @foreach($rubros as $rubro)
				            <?php                 
					            $codigo = $rubro -> iconRubro;
					            $array = explode(".", $codigo);
					        ?>
					        @if($array[0] === '00'.$j && $array[1]==='00'.$i && $array[2]==='000')
					        	<div class="form-group col-sm-2 rub {{'prod'.$j.$i}}" style="display: none;">
				              	{!! Form::label('codeProduct',$rubro -> descRubro.': ') !!}<br>
				            @endif
				            @if($array[0] === '00'.$j && $array[1]==='00'.$i && $array[2]!=='000')
				              	<input type="checkbox" name="rubro[]" value="{{ $rubro -> idRubro}}">{{ $rubro -> descRubro}}<br>
				            @endif		                
			            @endforeach
			            </div>
		            @endfor
	            @endfor
		        @for($i=1; $i<=2;$i++)  {{-- Talle --}}  
			        @foreach($rubros as $rubro)
				        <?php                 
					        $codigo = $rubro -> iconRubro;
					        $array = explode(".", $codigo);
					    ?>
					    @if($array[0] === '005' && $array[1]==='00'.$i && $array[2]==='000')
					      	<div class="form-group col-sm-2 rub {{'talle'.$i}}" style="display: none;">
				           	{!! Form::label('codeProduct','Talle '.$rubro -> descRubro.': ') !!}<br>
				        @endif
				        @if($array[0] === '005' && $array[1]==='00'.$i && $array[2]!=='000')
				        	@if($i==2)
				        		<input type="checkbox" class="col-sm-3" name="rubro[]" value="{{ $rubro -> idRubro}}">{{$rubro -> descRubro}}&nbsp;	
				          	@else
				          		<input type="checkbox" name="rubro[]" value="{{ $rubro -> idRubro}}">{{ $rubro -> descRubro}}
				          	@endif
				        @endif		                
			        @endforeach
			        </div>
		        @endfor
		        	  
			        @foreach($rubros as $rubro) {{-- Color --}} 
				        <?php                 
					        $codigo = $rubro -> iconRubro;
					        $array = explode(".", $codigo);
					    ?>
					    @if($array[0] === '007' && $array[1]==='000' && $array[2]==='000')
					      	<div class="form-group col-sm-2">
				           	{!! Form::label('codeProduct',$rubro -> descRubro.': ') !!}<br>
				        @endif
				        @if($array[0] === '007' && $array[1]!=='000' && $array[2]==='000')
				          	<input type="checkbox" name="rubro[]" value="{{ $rubro -> idRubro}}">{{ $rubro -> descRubro}}<br>
				        @endif		                
			        @endforeach
			        </div>
			        <div class="form-group col-sm-12">
	        	{!! Form::submit('Guardar Producto',['class' => 'btn palette-pumpkin']) !!} </div>      	
	        {!! Form::close() !!}
	        @endif
	        @endforeach
	    </div>
	</div>
	<script type="text/javascript" src="{{asset('js/Productcreate.js')}}"></script>
@endsection